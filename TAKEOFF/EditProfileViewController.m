////
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "EditProfileViewController.h"

@interface EditProfileViewController ()

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    //SideBar Added
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController setDelegate:self];
    if ( revealViewController )
    {
        [Btn_SideBar addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    Vw_Show.hidden = false;
    Vw_Edit.hidden = true;
    
    [self setupAlerts];
    [self CallMyMethod:YES];
    [self DatePickert];
    
    UITapGestureRecognizer *issueImage =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(IssueImage:)];
    [Img_User addGestureRecognizer:issueImage];
    Img_User.userInteractionEnabled = YES;
    Img_User.layer.cornerRadius = Img_User.frame.size.height/2;
    Img_User.layer.masksToBounds = YES;
    
    NSString *FirstName = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"FirstName"];
    
    NSString *LastName = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"LastName"];
    
    Lbl_UserName.text = [NSString stringWithFormat:@"%@ %@",FirstName, LastName];
    
    if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
    {
        NSLog(@"iphone5");
        Vw_Edit.frame = CGRectMake(Vw_Edit.frame.origin.x, Vw_Edit.frame.origin.y - 20, Vw_Edit.frame.size.width, Vw_Edit.frame.size.height + 70);
    }
}


-(void)DatePickert
{
    NSDate *date = [NSDate date];
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"MM/dd/yyyy"];
    NSString *result = [df stringFromDate:date];

    // alloc/init your date picker, and (optional) set its initial date
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]]; //this returns today's date
    
    NSString *maxDateString = result;
    // the date formatter used to convert string to date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // the specific format to use
    dateFormatter.dateFormat = @"MM-dd-yyyy";
    // converting string to date
    NSDate *theMaximumDate = [dateFormatter dateFromString: maxDateString];
    [datePicker setMaximumDate:theMaximumDate]; //the min age restriction
   
    // set the mode
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    
    // update the textfield with the date everytime it changes with selector defined below
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    
    // and finally set the datePicker as the input mode of your textfield
    [Txt_Birthdate setInputView:datePicker];

}

#pragma mark - Issue Image
//The event handling method
- (void)IssueImage:(UITapGestureRecognizer *)recognizer
{
    UIAlertController *alrtconditon=   [UIAlertController alertControllerWithTitle:@"Profile Image" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* tp = [UIAlertAction
                         actionWithTitle:@"Take Photo"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             ipc= [[UIImagePickerController alloc] init];
                             ipc.delegate = self;
                             ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
                             ipc.allowsEditing = YES;
                             [self presentViewController:ipc animated:YES completion:nil];
                             [alrtconditon dismissViewControllerAnimated:YES completion:nil];
                             
                             
                         }];
    
    [alrtconditon addAction:tp];
    
    UIAlertAction* cg = [UIAlertAction
                         actionWithTitle:@"Choose from Gallery"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             ipc= [[UIImagePickerController alloc] init];
                             ipc.delegate = self;
                             ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                             ipc.allowsEditing = YES;
                             [self presentViewController:ipc animated:YES completion:nil];
                             [alrtconditon dismissViewControllerAnimated:YES completion:nil];
                             
                             
                         }];
    
    [alrtconditon addAction:cg];
    
    UIAlertAction* can = [UIAlertAction
                          actionWithTitle:@"Cancel"
                          style:UIAlertActionStyleCancel
                          handler:^(UIAlertAction * action)
                          {
                              [alrtconditon dismissViewControllerAnimated:YES completion:nil];
                              
                          }];
    
    [alrtconditon addAction:can];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:alrtconditon];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        [self presentViewController:alrtconditon animated:YES completion:nil];
    }
}

#pragma mark - ImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // Dismiss the image selection, hide the picker and
    
    //show the image view with the picked image
    
    image = nil;
    image = [info objectForKey:UIImagePickerControllerEditedImage];
    if(image==nil)
    {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    if(image==nil)
    {
        image = [info objectForKey:UIImagePickerControllerCropRect];
    }
    
    Img_User.image = image;
    [ipc dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [ipc dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Method

// setupAlerts
-(void)setupAlerts{
    [Txt_Email addRegx:REGEX_EMAIL withMsg:@"Enter valid UserID."];
    [Txt_FirstName addRegx:REGEX_USER_NAME_LIMIT withMsg:@"Please enter valid firstname."];
    [Txt_LastName addRegx:REGEX_USER_NAME_LIMIT withMsg:@"Please enter valid lastname."];
}

#pragma mark - GetAllPosts

-(void)CallMyMethod :(BOOL)GetProfile
{
    [[AppDelegate sharedAppDelegate] showProgress];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:Txt_FirstName.text forKey:@"first_name"];
    [AddPost setValue:Txt_LastName.text forKey:@"last_name"];
    [AddPost setValue:Txt_Email.text forKey:@"email"];
    //[AddPost setValue:Txt_Birthdate.text forKey:@"confirm_new_password"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpEdit  = [[HttpWrapper alloc] init];
        httpEdit.delegate=self;
        
        NSLog(@"%@%@",JsonUrlConstant,ProfileAPI);
        
        if (GetProfile == YES){
            httpEdit.getbool=YES;
            dispatch_async( dispatch_get_main_queue(), ^{
                Vw_Show.hidden = false;
                Vw_Edit.hidden = true;
            });
            
            [httpEdit requestWithMethod:@"GET" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,ProfileAPI] param:nil];
        }
        else{
            httpEdit.getbool=NO;
            [httpEdit requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,ProfileAPI] param:[AddPost copy]];
            
            //profile
            
        }
    });
}

#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    NSLog(@"dict:%@",dicsResponse);
    
    [[AppDelegate sharedAppDelegate] hideProgress];
    if ([[dicsResponse valueForKey:@"status"] boolValue] == true) {
        
        if (Vw_Show.hidden == NO) {
            if (![[dicsResponse valueForKeyPath:@"data.first_name"] isEqual:[NSNull null]]) {
                Lbl_ShowUsername.text = [dicsResponse valueForKeyPath:@"data.first_name"];
                Txt_FirstName.text = [dicsResponse valueForKeyPath:@"data.first_name"];
            }
            if (![[dicsResponse valueForKeyPath:@"data.last_name"] isEqual:[NSNull null]]) {
                Lbl_ShowLastName.text = [dicsResponse valueForKeyPath:@"data.last_name"];
                Txt_LastName.text = [dicsResponse valueForKeyPath:@"data.last_name"];
            }
            if (![[dicsResponse valueForKeyPath:@"data.email"] isEqual:[NSNull null]]) {
                Lbl_ShowMail.text = [dicsResponse valueForKeyPath:@"data.email"];
                Txt_Email.text = [dicsResponse valueForKeyPath:@"data.email"];
            }
            if (![[dicsResponse valueForKeyPath:@"data.BirthDate"] isEqual:[NSNull null]]) {
                Lbl_ShowBirhtdate.text =[dicsResponse valueForKeyPath:@"data.BirthDate"];
                Txt_Birthdate.text =[dicsResponse valueForKeyPath:@"data.BirthDate"];
            }
        }
        else{
            [self CallMyMethod:YES];
        }
    }
    else
    {
        [[AppDelegate sharedAppDelegate] hideProgress];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
    }
}

//fetchDataFail
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
    [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:FAIL_DATA_MSG];
    NSLog(@"Fetch Data Fail Error:%@",error);
}


#pragma mark - SWRevealViewController Delegate

//revealController didMoveToPosition
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft)
    {
        revealController.frontViewController.view.alpha = 1;
        [self.revealViewController tapGestureRecognizer];
        self.view.userInteractionEnabled = YES;
    }
    else
    {
        revealController.frontViewController.view.alpha = 0.5;
        
        self.view.userInteractionEnabled = NO;
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
}

- (IBAction)DoneButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if ([Txt_FirstName validate] || [Txt_LastName validate] || [Txt_Email validate]) {
        [self CallMyMethod:NO];
    }
}

- (IBAction)EditButtonPressed:(id)sender {
    Vw_Show.hidden = true;
    Vw_Edit.hidden = false;
}



#pragma mark - Picker Delegate
-(void)updateTextField:(id)sender {
    
    UIDatePicker *picker = (UIDatePicker*)Txt_Birthdate.inputView;
    Txt_Birthdate.text = [self formatDate:picker.date];
}

- (NSString *)formatDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}


@end
