//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>
// SideBar Class 3rd Party
#import "SWRevealViewController.h"
#import "SidebarViewController.h"
#import "RegisterViewController.h"
#import "HttpWrapper.h"
#import "AppDelegate.h"
#import "TextFieldValidator.h"
#import "StudyViewController.h"

@interface LoginViewController : UIViewController<SWRevealViewControllerDelegate,HttpWrapperDelegate>
{
    //WrapperClass Object
    HttpWrapper *httplogin;
    
    //Outlets
    
    //TextFeild
    IBOutlet TextFieldValidator *Txt_UserID;
    IBOutlet TextFieldValidator *Txt_Password;
    
    //Label
    IBOutlet UILabel *Lbl_CreateAcc;
    
    //View
    IBOutlet UIView *Vw_Login;
    
}

@property (strong, nonatomic) SWRevealViewController *viewController;
    
//Action
- (IBAction)ForgotButtonPressed:(id)sender;
- (IBAction)LoginButtonPressed:(id)sender;
    
    
@end
