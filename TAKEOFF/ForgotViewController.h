//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "AppDelegate.h"

@interface ForgotViewController : UIViewController<HttpWrapperDelegate>
{
    //WrapperClass Object
    HttpWrapper *httpforgot;
    
    //Outlets
    
    //View
    IBOutlet UIView *Vw_Back;
    
    //TextFeild
    IBOutlet TextFieldValidator *Txt_UserID;
    
}
    
//Action
- (IBAction)SendButtonPressed:(id)sender;
    

@end
