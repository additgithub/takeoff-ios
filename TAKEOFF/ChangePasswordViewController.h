//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "AppDelegate.h"
#import "TextFieldValidator.h"

@interface ChangePasswordViewController : UIViewController<HttpWrapperDelegate,SWRevealViewControllerDelegate>
{
    //WrapperClass Object
    HttpWrapper *httpchange;
    
    //Outlets
    
    //View
    IBOutlet UIView *Vw_Back;
    
    IBOutlet TextFieldValidator *Txt_CurrentPass;
    IBOutlet TextFieldValidator *Txt_NewPass;
    IBOutlet TextFieldValidator *Txt_ConfirmPass;
}

- (IBAction)ChangeButtonPressed:(id)sender;

@end
