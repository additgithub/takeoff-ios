//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "SidebarViewController.h"

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

@interface SidebarViewController ()

@end

@implementation SidebarViewController


#pragma mark - Happy Coding

//ViewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    FirstChange = NO;
    Change = NO;
    
    //NavigationBar
    self.navigationController.navigationBar.hidden = YES;
    [self.navigationItem setHidesBackButton:YES];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        _Img_UserImg.frame = CGRectMake(_Img_UserImg.frame.origin.x - 10 , _Img_UserImg.frame.origin.y +10, 90, 90);
        [_Btn_Profile setFrame:CGRectMake(_Btn_Profile.frame.origin.x +15, _Btn_Profile.frame.origin.y - 15, _Btn_Profile.frame.size.width, _Btn_Profile.frame.size.height)];
    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812.0f){
            NSLog(@"iPhone X");
            _Img_UserImg.frame = CGRectMake(_Img_UserImg.frame.origin.x - 10 , _Img_UserImg.frame.origin.y +10, 90, 90);
            _Btn_Profile.frame = CGRectMake(_Btn_Profile.frame.origin.x, _Btn_Profile.frame.origin.y, _Btn_Profile.frame.size.width, _Btn_Profile.frame.size.height);
            _Lbl_Username.frame = CGRectMake(_Lbl_Username.frame.origin.x, _Lbl_Username.frame.origin.y + 10, _Lbl_Username.frame.size.width, _Lbl_Username.frame.size.height);
            _Lbl_UserEmail.frame = CGRectMake(_Lbl_UserEmail.frame.origin.x, _Lbl_UserEmail.frame.origin.y + 10, _Lbl_UserEmail.frame.size.width, _Lbl_UserEmail.frame.size.height);
            
            Btn_Setting.frame = CGRectMake(Btn_Setting.frame.origin.x, Btn_Setting.frame.origin.y + 20, Btn_Setting.frame.size.width, Btn_Setting.frame.size.height);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
        {
            NSLog(@"iphone5");
            _Img_UserImg.frame = CGRectMake(_Img_UserImg.frame.origin.x, _Img_UserImg.frame.origin.y, 70, 70);
            _Lbl_Username.frame = CGRectMake(_Lbl_Username.frame.origin.x, _Lbl_Username.frame.origin.y + 10, _Lbl_Username.frame.size.width, _Lbl_Username.frame.size.height);
            _Lbl_UserEmail.frame = CGRectMake(_Lbl_UserEmail.frame.origin.x, _Lbl_UserEmail.frame.origin.y + 10, _Lbl_UserEmail.frame.size.width, _Lbl_UserEmail.frame.size.height);
        }
        else
        {
            _Img_UserImg.frame = CGRectMake(_Img_UserImg.frame.origin.x, _Img_UserImg.frame.origin.y, 70, 70);
        }
    }
    else
    {
        _Img_UserImg.frame = CGRectMake(_Img_UserImg.frame.origin.x, _Img_UserImg.frame.origin.y, 70, 70);
    }
    
    
    
    
}

//ViewWillAppear
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    //Gradiant View
    //Gradiant View
    //[[ApplicationData sharedInstance] GradiantColorUsing:self.view];
    
    //Button Profile Usage
    // Shadow and Radius
    _Btn_Profile.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.25f] CGColor];
    _Btn_Profile.layer.shadowOffset = CGSizeMake(0, 2.0f);
    _Btn_Profile.layer.shadowOpacity = 1.0f;
    _Btn_Profile.layer.shadowRadius = 0.0f;
    _Btn_Profile.layer.masksToBounds = NO;
    _Btn_Profile.layer.cornerRadius = 15.0f;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        _Lbl_Username.font = [UIFont fontWithName:@"Roboto-Medium" size:18];
        _Lbl_UserEmail.font = [UIFont fontWithName:@"Roboto-Medium" size:12];
        
    }
    
    _Img_UserImg.layer.cornerRadius = _Img_UserImg.frame.size.width / 2;
    _Img_UserImg.clipsToBounds = YES;
    _Img_UserImg.layer.borderWidth = 3.0f;
    _Img_UserImg.layer.borderColor = [UIColor colorWithRed:218/225.0f green:246/255.0f blue:255/255.0f alpha:1].CGColor;
    
    if (FirstChange == NO) {
        FirstChange = YES;
        Change = YES;
        
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
//            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
//            if (screenSize.height == 812.0f){
//                NSLog(@"iPhone X");
//                _Img_UserImg.frame = CGRectMake(_Img_UserImg.frame.origin.x, _Img_UserImg.frame.origin.y + 10, _Img_UserImg.frame.size.width, _Img_UserImg.frame.size.height);
//            }
//            else{
//                _Img_UserImg.frame = CGRectMake(_Img_UserImg.frame.origin.x+5, _Img_UserImg.frame.origin.y + 10, _Img_UserImg.frame.size.width, _Img_UserImg.frame.size.height);
//            }
//        }
    }
    
    menuItems0 = [[NSArray alloc] init];
    menuItems = @[@"Home", @"My Profile", @"My Tests", @"Change Password",  @"Logout"];
    
    NSLog(@"menuItems4 %@",menuItems4);
    
    NSString *FirstName = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"FirstName"];
    
    NSString *LastName = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"LastName"];
    
    NSString *Email = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"Email"];
    
    _Lbl_Username.text = [NSString stringWithFormat:@"%@ %@",FirstName, LastName];
    
    _Lbl_UserEmail.text = Email;
    
    [_Tbl_Main reloadData];
}


#pragma mark - Table view data source

//numberOfSectionsInTableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 4;
}

//numberOfRowsInSection
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    if (section == 0) {
        return menuItems0.count;
    }
    if (section == 1) {
        return menuItems.count;
    }
    if (section == 2) {
        return menuItems2.count;
    }
    if (section == 3) {
        return menuItems3.count;
    }
    else
    {
        return menuItems4.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 50;
    }
    else
    {
        return 37;
    }
}

//cellForRowAtIndexPath
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    UILabel *Lbl_Items = (UILabel *)[cell viewWithTag:100];
    UILabel *SideCount = (UILabel *)[cell viewWithTag:200];
    UIImageView *Img = (UIImageView *)[cell viewWithTag:101];
    
    SideCount.hidden = YES;
    if (indexPath.section == 0) {
        Lbl_Items.text = menuItems0[indexPath.row];
        Img.image = [UIImage imageNamed:menuItems0[indexPath.row]];
    }
    else if (indexPath.section == 1) {
        Lbl_Items.text = menuItems[indexPath.row];
        Img.image = [UIImage imageNamed:menuItems[indexPath.row]];
    }
    else if (indexPath.section == 2) {
        Lbl_Items.text = menuItems2[indexPath.row];
        Img.image = [UIImage imageNamed:menuItems2[indexPath.row]];
    }
    else if (indexPath.section == 3) {
        Lbl_Items.text = menuItems3[indexPath.row];
        Img.image = [UIImage imageNamed:menuItems3[indexPath.row]];
    }
    else if (indexPath.section == 4) {
        NSLog(@"menuItems4 %@",menuItems4);
        NSLog(@"Lbl_Items.text %@",Lbl_Items.text);
        Lbl_Items.text = menuItems4[indexPath.row];
        Img.image = [UIImage imageNamed:menuItems4[indexPath.row]];
    }
    
    NSLog(@"lbl_items %@",Lbl_Items.text);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        Lbl_Items.font = [UIFont fontWithName:@"Roboto-Regular" size:18];
    }
    
    return cell;
}

//didSelectRowAtIndexPath
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"I'm selected %ld",(long)indexPath.row);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger row = indexPath.row;
    
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    newFrontController = nil;

    if (indexPath.section == 1) {
        switch (row) {
            case 0:
            {
                StudyViewController *change = [self.storyboard instantiateViewControllerWithIdentifier:@"StudyViewController"];
                newFrontController = change;
                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                [revealController pushFrontViewController:navigationController animated:YES];
            }
                break;
            case 1:
            {
                EditProfileViewController *change = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
                newFrontController = change;
                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                [revealController pushFrontViewController:navigationController animated:YES];
            }
                break;
            case 2:
            {
                MyTestViewController *change = [self.storyboard instantiateViewControllerWithIdentifier:@"MyTestViewController"];
                newFrontController = change;
                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                [revealController pushFrontViewController:navigationController animated:YES];
            }
                break;
            case 3:
            {
                ChangePasswordViewController *change = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
                newFrontController = change;
                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                [revealController pushFrontViewController:navigationController animated:YES];
            }
                break;
//            case 4:
//            {
//                WebViewController *webview = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
//
//                newFrontController = webview;
//                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
//                [revealController pushFrontViewController:navigationController animated:YES];
//            }
//                break;
            case 4:
            {
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Logout"
                                             message:@"Are you sure you want to logout?"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                //Add Buttons
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"Yes"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                
                                                FirstChange = NO;
                                                
                                                NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
                                                [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];

                                                
                                                newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                                
                                                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                                                [revealController pushFrontViewController:navigationController animated:YES];
                                            }];
                
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"Cancel"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               //Handle no, thanks button
                                           }];
                
                //Add your buttons to alert controller
                
                [alert addAction:yesButton];
                [alert addAction:noButton];
                
                [self presentViewController:alert animated:YES completion:nil];
                
            }
                break;
            default:
                break;
        }
    }
}


@end
