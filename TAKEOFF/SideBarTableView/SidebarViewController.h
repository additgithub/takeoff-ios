//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>

// SideBar Class 3rd Party
#import "SWRevealViewController.h"
#import "ChangePasswordViewController.h"
#import "WebViewController.h"
#import "EditProfileViewController.h"
#import "MyTestViewController.h"

@interface SidebarViewController : UIViewController
{
    BOOL Change;
    BOOL FirstChange;
    
    //Array Declaration
    NSArray *menuItems0;
    NSArray *menuItems;
    NSArray *menuItems2;
    NSArray *menuItems3;
    NSArray *menuItems4;
    IBOutlet UIButton *Btn_Setting;
    
    UIViewController *newFrontController;
}

//Outlets
@property (strong, nonatomic) IBOutlet UITableView *Tbl_Main;
@property (strong, nonatomic) IBOutlet UIButton *Btn_Profile;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_Username;
@property (strong, nonatomic) IBOutlet UIImageView *Img_UserImg;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_UserEmail;

@end
