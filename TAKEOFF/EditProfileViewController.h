////
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldValidator.h"
#import "AppDelegate.h"
#import "HttpWrapper.h"
#import "SidebarViewController.h"

@interface EditProfileViewController : UIViewController<SWRevealViewControllerDelegate,HttpWrapperDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    
    //Image
    UIImage *image;
    
    //ImagePicker
    UIImagePickerController*ipc;
    
    //WrapperClass Object
    HttpWrapper *httpEdit;
    
    //DatePicker
    UIDatePicker *datePicker;
    UIPickerView *pickerView;
    NSMutableArray*years;
    
    //Outlets
    
    //View
    IBOutlet UIView *Vw_Edit;
    IBOutlet UIView *Vw_Show;
    
    //TextFeild
    IBOutlet TextFieldValidator *Txt_FirstName;
    IBOutlet TextFieldValidator *Txt_LastName;
    IBOutlet UITextField *Txt_Birthdate;
    IBOutlet TextFieldValidator *Txt_Email;
    
    //ImageView
    IBOutlet UIImageView *Img_User;
    
    //Label
    IBOutlet UILabel *Lbl_UserName;
    
    //LabelShowProfile
    IBOutlet UILabel *Lbl_ShowUsername;
    IBOutlet UILabel *Lbl_ShowLastName;
    IBOutlet UILabel *Lbl_ShowBirhtdate;
    IBOutlet UILabel *Lbl_ShowMail;
    
    
    //Button
    IBOutlet UIButton *Btn_SideBar;
}

//Action
- (IBAction)DoneButtonPressed:(id)sender;
- (IBAction)EditButtonPressed:(id)sender;



@end
