////
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "HttpWrapper.h"
#import "CVCDispQuestions.h"
#import "CVCQuestionNo.h"
#import "AppDelegate.h"

@interface TestQuestionViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,UIWebViewDelegate,SWRevealViewControllerDelegate,HttpWrapperDelegate>
{
    //WrapperClass Object
    HttpWrapper *httpQuestion,*httpSubmit;
    
    BOOL gotoFlag;
    
    //Array Allocation
    NSMutableArray *AddNew;
    NSMutableArray *AddNew2;
    NSMutableArray *GETQuestions;
    NSMutableArray *GETOptions;
    
    NSMutableArray *marrFlag;
    NSMutableArray *marrFlag1;
    NSMutableArray *marrFlag2;
    NSMutableArray *arrAttamt;
    
    //View
    IBOutlet UIView *GetView;
    IBOutlet UIView *ExitView;
    
    //Label
    IBOutlet UILabel *Lbl_Score;
    
    NSString * strUrl;
    NSString * strQuestion , * strOption1 , * strOption2 ,* strOption3 , * strOption4;
    CVCDispQuestions * cell;
    int curruntRow;
    int lastHeaderID;
    NSUInteger attempted;
    bool isLoaded;
    BOOL myturn;
    
}

@property (nonatomic, assign) CGFloat lastContentOffset;

@property (retain,nonatomic) NSString *GetTestHeaderId;
@property (retain,nonatomic) NSString *GetQuestionNumber;
@property (assign) BOOL GETSTUDY;
@property(nonatomic,assign)NSString * GetChapterID;
@property(nonatomic,assign)NSString * subjectId;
@property(nonatomic,assign)NSString * subjectName;
@property(nonatomic,assign)NSString * chepterId;
@property(nonatomic,assign)int levelId;



@property (weak, nonatomic) IBOutlet UICollectionView *collDispQuestions;
@property (weak, nonatomic) IBOutlet UICollectionView *collDispQuestionNo;


@property (weak, nonatomic) IBOutlet UILabel *lblUserName;



@property (weak, nonatomic) IBOutlet UIButton *btnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblDispQuestionNo;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;


- (IBAction)btnBack:(id)sender;
- (IBAction)bntPrevious:(id)sender;
- (IBAction)btnNext:(id)sender;
- (IBAction)btnSubmit:(id)sender;

- (IBAction)SaveandExitButtonPressed:(id)sender;
- (IBAction)ExitButtonPressed:(id)sender;
- (IBAction)ExitQuizButtonPressed:(id)sender;
@end
