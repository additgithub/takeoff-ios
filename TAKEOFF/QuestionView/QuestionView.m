//
//  QuestionView.m
//  McqApp
//
//  Created by My Mac on 3/9/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#import "QuestionView.h"
#import "CVCDispQuestions.h"
#import "CVCQuestionNo.h"
#import "AppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AFTPagingBaseViewController.h"


@interface QuestionView ()
{
    CGFloat op1;
    CGFloat op2;
    CGFloat op3;
    CGFloat op4;
    
}
@property (nonatomic, assign) CGFloat lastContentOffset;

@end

NSString * strUrl;
NSString * strQuestion , * strOption1 , * strOption2 ,* strOption3 , * strOption4;
CVCDispQuestions * cell;
@implementation QuestionView

int curruntRow;
int lastHeaderID;
NSUInteger attempted;
bool isLoaded = false;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = _GetChapterName;
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:46/255.0 green:151/255.0 blue:230/255.0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Semibold" size:17.0]}];
    
    UIButton *dButton=[UIButton buttonWithType:0];
    dButton.frame=CGRectMake(50,50,20,20);
    [dButton setImage:[UIImage imageNamed:@"info"]
             forState:UIControlStateNormal];
    dButton.adjustsImageWhenHighlighted=NO;
    dButton.adjustsImageWhenDisabled=NO;
    dButton.tag=0;
    dButton.backgroundColor=[UIColor clearColor];
    [dButton addTarget:self action:@selector(infoviewOpen:)
      forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *RightButton=[[UIBarButtonItem alloc] initWithCustomView:dButton];
    self.navigationItem.rightBarButtonItem=RightButton;
    
   
    /*
    //SideBar Added
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController setDelegate:self];
    if ( revealViewController )
    {
        [dButton addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }*/
    
    _lblUserName.text = @"Username";
    
    curruntRow = 0;
    
    _btnPrevious.enabled = false;
    
    cell.scrollVQuestion.delegate = self;
    
   
    marrFlag1=[[NSMutableArray alloc]init];
    
    ExitView.layer.cornerRadius = 5.0;
    ExitView.layer.masksToBounds = true;
    
    //The setup code (in viewDidLoad in your view controller)
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [GetView addGestureRecognizer:singleFingerTap];
    
    //The setup code (in viewDidLoad in your view controller)
    UITapGestureRecognizer *singleFingerTap2 =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap2:)];
    [InfoView addGestureRecognizer:singleFingerTap2];
    
    [self CallMyMethod];
    
    //arrAttamt=[[NSMutableArray alloc]init];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
}


-(IBAction)infoviewOpen:(id)sender
{
    //Your code here
    NSLog(@"getinfo");
    InfoView.hidden = NO;
}


-(void)CallMyMethod
{
    [[AppDelegate sharedAppDelegate] showProgress];
    
    NSString *UserID = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"UserID"];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    if (_GETSTUDY == YES) {
        [AddPost setValue:_GetChapterID forKey:@"chapter_id"];
        [AddPost setValue:_GetQuestionNumber forKey:@"no_of_questions"];
        [AddPost setValue:UserID forKey:@"user_id"];
    }
    else{
        [AddPost setValue:_GetChapterID forKey:@"no_of_questions"];
        [AddPost setValue:UserID forKey:@"user_id"];
    }
    
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpQuestion  = [[HttpWrapper alloc] init];
        httpQuestion.delegate=self;
        httpQuestion.getbool=NO;
        if (_GETSTUDY == YES) {
            [httpQuestion requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,GETStudyQuestionAPI] param:[AddPost copy]];
        }
        else{
            [httpQuestion requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,GETTestQuestionAPI] param:[AddPost copy]];
        }
    });
    
}

#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    NSLog(@"dict:%@",dicsResponse);
    
    [[AppDelegate sharedAppDelegate] hideProgress];
    if ([[dicsResponse valueForKey:@"status"] boolValue] == true) {
        GETQuestions = [dicsResponse valueForKey:@"data"];
        GETOptions = [[NSMutableArray alloc] init];
        GETOptions = [GETQuestions valueForKey:@"options"];
        
         marrFlag=[[NSMutableArray alloc] init];
        arrAttamt=[[NSMutableArray alloc]init];
        for (int i = 0; i<GETOptions.count; i++) {
            [marrFlag addObject:[NSString stringWithFormat:@"0"]];
            [arrAttamt addObject:@"0"];
        }
        NSLog(@"marrflag %@",marrFlag);
        
        
        NSMutableAttributedString *CreateAcc;
        
        NSRange linkRange2;
        if (GETQuestions.count <= 9) {
            CreateAcc = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attempt 00 / %lu",GETQuestions.count] attributes:nil];
            NSString *AccString = [NSString stringWithFormat:@"Attempt 00 / %lu",GETQuestions.count];
            linkRange2 = [AccString rangeOfString:[NSString stringWithFormat:@"00"]];
        }
        else{
            CreateAcc = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attempt 00 / %lu",GETQuestions.count] attributes:nil];
            NSString *AccString = [NSString stringWithFormat:@"Attempt 00 / %lu",GETQuestions.count];
            linkRange2 = [AccString rangeOfString:[NSString stringWithFormat:@"00"]];
        }
        
        NSDictionary *linkAttributes2 = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:0.05 green:0.4 blue:0.65 alpha:1.0]};
        [CreateAcc setAttributes:linkAttributes2 range:linkRange2];
        
        
        
        // Assign attributedText to UILabel
        _lblDispQuestionNo.attributedText = CreateAcc;
        
        [self.collDispQuestionNo reloadData];
        [self.collDispQuestions reloadData];
    }
    else
    {
        [[AppDelegate sharedAppDelegate] hideProgress];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
    }
}

//fetchDataFail
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
    [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:FAIL_DATA_MSG];
    NSLog(@"Fetch Data Fail Error:%@",error);
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    GetView.hidden = YES;
    //Do stuff here...
}

//The event handling method
- (void)handleSingleTap2:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    InfoView.hidden = YES;
    //Do stuff here...
}

-(void)viewWillLayoutSubviews{

    self.btnSubmit.layer.borderWidth = 1.0;
    self.btnSubmit.layer.cornerRadius = 5 ;
    self.btnSubmit.layer.borderColor = [[UIColor lightGrayColor]CGColor];

}


#pragma mark - UICollectionView Method


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
   
    return GETQuestions.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSString *selectedAns1 =[GETOptions valueForKey:@"is_selected"][indexPath.row][0] ;
    NSString *selectedAns2 =[GETOptions valueForKey:@"is_selected"][indexPath.row][1] ;
    NSString *selectedAns3 =[GETOptions valueForKey:@"is_selected"][indexPath.row][2] ;
    NSString *selectedAns4 =[GETOptions valueForKey:@"is_selected"][indexPath.row][3] ;
    
    NSString *rightAns1 =[GETOptions valueForKey:@"Answer"][indexPath.row][0];
    NSString *rightAns2 =[GETOptions valueForKey:@"Answer"][indexPath.row][1];
    NSString *rightAns3 =[GETOptions valueForKey:@"Answer"][indexPath.row][2];
    NSString *rightAns4 =[GETOptions valueForKey:@"Answer"][indexPath.row][3];
    
    
    if(collectionView == self.collDispQuestionNo)
    {
        
        CVCQuestionNo *cellNo = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellQuestionNo" forIndexPath:indexPath];
        
        cellNo.lblQuestionNo.text = [NSString stringWithFormat:@"%ld",indexPath.row + 1];
        
        
        cellNo.btn_flag1.layer.cornerRadius = cellNo.btn_flag1.frame.size.height/2;
        cellNo.btn_flag1.layer.masksToBounds = YES;
        cellNo.btn_flag1.tag=5;
        
        if ([[marrFlag objectAtIndex:indexPath.row] intValue]==0) {
            NSLog(@"i'm calling1");
            cellNo.btn_flag1.hidden = YES;
            //cellNo.lblSelection.backgroundColor = [UIColor redColor];
        }
        else
        {
            NSLog(@"i'm calling");
            cellNo.btn_flag1.hidden = NO;
            if ([[marrFlag objectAtIndex:indexPath.row] intValue]==2) {
                NSLog(@"i'm calling2");
                cellNo.btn_flag1.selected = YES;
                //cellNo.lblSelection.backgroundColor = [UIColor redColor];
            }
            else{
                NSLog(@"i'm calling3");
                cellNo.btn_flag1.selected = NO;
                //cellNo.lblSelection.backgroundColor = [UIColor greenColor];
            }
        }
        if (![selectedAns1  isEqualToString:@"0"]){
            if ([rightAns1 isEqualToString:@"1"]) {
                cellNo.lblSelection.backgroundColor = [UIColor greenColor];
            }
            else{
                cellNo.lblSelection.backgroundColor = [UIColor redColor];
            }
        }
        else if (![selectedAns2  isEqualToString:@"0"]){
            if ([rightAns2 isEqualToString:@"1"]) {
                cellNo.lblSelection.backgroundColor = [UIColor greenColor];
            }
            else{
                cellNo.lblSelection.backgroundColor = [UIColor redColor];
            }
        }
        else if (![selectedAns3  isEqualToString:@"0"]){
            if ([rightAns3  isEqualToString:@"1"]) {
                cellNo.lblSelection.backgroundColor = [UIColor greenColor];
            }
            else{
                cellNo.lblSelection.backgroundColor = [UIColor redColor];
            }
        }
        else if (![selectedAns4  isEqualToString:@"0"]){
            if ([rightAns4  isEqualToString:@"1"]) {
                cellNo.lblSelection.backgroundColor = [UIColor greenColor];
            }
            else{
                cellNo.lblSelection.backgroundColor = [UIColor redColor];
            }
        }
        else{
            cellNo.lblSelection.backgroundColor = [UIColor whiteColor];
        }
        
        
        return cellNo;
        
    }
    else
    {
        
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        
        cell.viewQuestionHeight.constant = 60.0;
        cell.viewOption1Height.constant = 60.0;
        cell.viewOption2Height.constant = 60.0;
        cell.viewOption3Height.constant = 60.0;
        cell.viewOption4Height.constant = 60.0;
        
        cell.Lbl_PageNumber.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
        
        if (indexPath.row+1 <= 9) {
            cell.Lbl_PageNumber.text = [NSString stringWithFormat:@"0%ld.",indexPath.row+1];
        }
        else{
            if (indexPath.row+1 > 9) {
                cell.Lbl_PageNumber.text = [NSString stringWithFormat:@"%ld.",indexPath.row+1];
            }
            else{
                cell.Lbl_PageNumber.text = [NSString stringWithFormat:@"0%ld.",indexPath.row+1];
            }
        }
        
        cell.Lbl_PageNumber.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
        cell.btn_flag.userInteractionEnabled = YES;
        
        CGFloat QHeight=[self getLabelHeight:cell.txtVQuestion];
       // CGSize maxSize = CGSizeMake(cell.txtVQuestion.frame.size.width, 1000);
       // CGRect labrect = [[GETQuestions valueForKey:@"QuestionName"][indexPath.row] boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:cell.txtVQuestion.font} context:Nil];
        cell.txtVQuestion.text = [GETQuestions valueForKey:@"QuestionName"][indexPath.row];
       // label.text = lorum;
        //for use UITextView you should comment the line under
       // label.numberOfLines = 0;
       // cell.txtVQuestion.frame = CGRectMake(cell.txtVQuestion.frame.origin.x, cell.txtVQuestion.frame.origin.y, cell.txtVQuestion.frame.size.width, labrect.size.height);
        // [cell.txtVQuestion sizeToFit];
        [cell.txtVQuestion  setScrollEnabled:NO];
        CGFloat fixedWidth = cell.txtVQuestion.frame.size.width;
        CGSize newSize = [cell.txtVQuestion sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = cell.txtVQuestion.frame;
        newFrame.size = CGSizeMake(fmaxf(newFrame.size.width, fixedWidth), newSize.height);
        cell.txtVQuestion.frame = newFrame;
        [cell.txtVQuestion layoutIfNeeded];
        [cell layoutIfNeeded];
//        [cell.txtVQuestion setContentMode:UIViewContentModeScaleToFill];
//        CGRect QH = cell.txtVQuestion.frame;
//        QH.size.height = QH.size.height;
//        cell.txtVQuestion.frame = QH;
        
//        CGRect QH1 = cell.viewDispQuestion.frame;
//        QH1.size.width = QH1.size.width;
//        cell.txtVQuestion.frame = QH;
       
       // NSData *imageData = [NSData dataWithContentsOfURL:imgURL];
       // self.imageView.image = [UIImage imageWithData:imageData];
        
        //cell.txtVQuestion.contentInset = UIEdgeInsetsMake(0,10,0,10);
        
//        NSLog(@"GET QUESTION %@",[GETQuestions valueForKeyPath:@"options.OptionDescription"][indexPath.row][1]);
//        NSLog(@"GET QUESTION %@",[GETQuestions valueForKeyPath:@"options.OptionDescription"][indexPath.row][2]);
//        NSLog(@"GET QUESTION %@",[GETQuestions valueForKeyPath:@"options.OptionDescription"][indexPath.row][3]);
        
        cell.txtVOption1.text = [NSString stringWithFormat:@"  %@",[GETOptions valueForKey:@"OptionDescription"][indexPath.row][0]];
        NSLog(@"test = %@",[GETOptions valueForKey:@"OptionDescription"][indexPath.row][0]);
        if ([cell.txtVOption1.text isEqualToString:@"  "]) {
            cell.txtVOption1.hidden = YES;
            cell.lblA.hidden = YES;
        }
        else{
            cell.txtVOption1.hidden = NO;
            cell.lblA.hidden = NO;
        }
        
        cell.txtVOption2.text = [NSString stringWithFormat:@"  %@",[GETOptions valueForKey:@"OptionDescription"][indexPath.row][1]];
        if ([cell.txtVOption2.text isEqualToString:@"  "]) {
            cell.txtVOption2.hidden = YES;
            cell.lblB.hidden = YES;
        }
        else{
            cell.txtVOption2.hidden = NO;
            cell.lblB.hidden = NO;
        }
        
        cell.txtVOption3.text = [NSString stringWithFormat:@"  %@",[GETOptions valueForKey:@"OptionDescription"][indexPath.row][2]];
        if ([cell.txtVOption3.text isEqualToString:@"  "]) {
            cell.txtVOption3.hidden = YES;
            cell.lblC.hidden = YES;
        }
        else{
            cell.txtVOption3.hidden = NO;
            cell.lblC.hidden = NO;
        }
        
        cell.txtVOption4.text = [NSString stringWithFormat:@"  %@",[GETOptions valueForKey:@"OptionDescription"][indexPath.row][3]];
        if ([cell.txtVOption4.text isEqualToString:@"  "]) {
            cell.txtVOption4.hidden = YES;
            cell.lblD.hidden = YES;
        }
        else{
            cell.txtVOption4.hidden = NO;
            cell.lblD.hidden = NO;
        }
        
        op1=[self getLabelHeight:cell.txtVOption1];
        op2=[self getLabelHeight:cell.txtVOption2];
        op3=[self getLabelHeight:cell.txtVOption3];
        op4=[self getLabelHeight:cell.txtVOption4];
        
        NSString * str1 = [NSString stringWithFormat:@"%f",op1];
        NSString * str2 = [NSString stringWithFormat:@"%f",op2];
        NSString * str3 = [NSString stringWithFormat:@"%f",op3];
        NSString * str4 = [NSString stringWithFormat:@"%f",op4];
        NSLog(@"str1 = %@",str1);
        NSLog(@"str2 = %@",str2);
        NSLog(@"str3 = %@",str3);
        NSLog(@"str4 = %@",str4);
        
        
        
        NSString *imgURL =  [GETQuestions valueForKey:@"ImageName"][indexPath.row];
        
        NSURL *url = [NSURL URLWithString:imgURL];
        
        //if(![imgURL isEqualToString:@""])
        if (url && url.scheme && url.host)
        {
            cell.img_question.hidden=NO;
            cell.btn_zoom.hidden=NO;
            
            cell.btn_zoom.tag = indexPath.row;
            cell.btn_zoom.userInteractionEnabled = YES;
            [cell.btn_zoom addTarget:self action:@selector(zoom_img:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.img_question.userInteractionEnabled=YES;
            [cell.img_question sd_setImageWithURL:[NSURL URLWithString:imgURL]
                                 placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            
            
            
          /*  UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
            letterTapRecognizer.numberOfTapsRequired = 1;
            [cell.img_question addGestureRecognizer:letterTapRecognizer];
            */
            
            CGRect imageRect = CGRectMake(cell.img_question.frame.origin.x, cell.img_question.frame.origin.y, cell.img_question.frame.size.width, cell.img_question.frame.size.height);
                                          
            NSLog(@"imageRect = %@",NSStringFromCGRect(imageRect));
            
          //  if(cell.viewOption1.frame.origin.y != 352) {
                CGRect frame1 = cell.viewOption1.frame;
                  frame1.origin.y = 352;
            frame1.size.height = op1;
                cell.viewOption1.frame = frame1;
            
          //  }
            
         //   if(cell.viewOption2.frame.origin.y != 412) {
                CGRect frame2 = cell.viewOption2.frame;
                frame2.origin.y = 412;
                cell.viewOption2.frame = frame2;
          //  }
            
         //   if(cell.viewOption3.frame.origin.y != 469) {
                CGRect frame3 = cell.viewOption3.frame;
                frame3.origin.y = 469;
                cell.viewOption3.frame = frame3;
           // }
            
         //   if(cell.viewOption4.frame.origin.y != 528) {
                CGRect frame4 = cell.viewOption4.frame;
                frame4.origin.y = 528;
                cell.viewOption4.frame = frame4;
         //   }
           
            cell.scrollVQuestion.contentSize =CGSizeMake(cell.contentView.frame.size.width, 680);
            cell.scrollVQuestion.delegate=nil;
            CGRect frame12 = cell.contentView.frame;
            frame12.size.height = 680;
            cell.Vw_Back.frame = frame12;
        }
        else
        {
            cell.btn_img_zoom.hidden=YES;
            cell.img_question.hidden=YES;
            
            
            if(cell.viewOption1.frame.origin.y != 500)
            {
                CGRect frame1 = cell.viewOption1.frame;
                //frame1.origin.y = 202;
                CGFloat op123 =[self getLabelHeight:cell.txtVOption1];
                frame1.origin.y = cell.txtVQuestion.frame.size.height+30*2;
                 frame1.size.height = op1;
                
               // cell.btnOption1.frame = cell.viewOption1.frame;
                // [cell.lblA setFrame:CGRectMake(cell.lblA.frame.origin.x, frame1.origin.y, cell.lblA.frame.size.width, cell.lblA.frame.size.height)];
//                frame1.origin.y = cell.txtVQuestion.frame.size.height +op123+20*2;
                cell.viewOption1.frame = frame1;
                NSString * op123kkkk = [NSString stringWithFormat:@"%f",op123];
                NSLog(@"op123kkkk = %@",op123kkkk);
               
            }
            
            if(cell.viewOption2.frame.origin.y != 500)
            {
                CGRect frame2 = cell.viewOption2.frame;
                //frame2.origin.y = 260;
                CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
                frame2.size.height = op2;
               // cell.bntOption2.frame = cell.viewOption2.frame;
                //[cell.lblB setFrame:CGRectMake(cell.lblA.frame.origin.x, frame2.origin.y, cell.lblA.frame.size.width, cell.lblA.frame.size.height)];
                frame2.origin.y = cell.txtVQuestion.frame.size.height + cell.viewOption1.frame.size.height+30*2+10;
                cell.viewOption2.frame = frame2;
            }
            
            if(cell.viewOption3.frame.origin.y != 500)
            {
                CGRect frame3 = cell.viewOption3.frame;
                //frame3.origin.y = 317;
                CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
                 frame3.size.height = op3;
               // cell.btnOption3.frame = cell.viewOption3.frame;
                //[cell.lblC setFrame:CGRectMake(cell.lblA.frame.origin.x, frame3.origin.y, cell.lblA.frame.size.width, cell.lblA.frame.size.height)];
                frame3.origin.y = cell.txtVQuestion.frame.size.height + cell.viewOption1.frame.size.height +cell.viewOption2.frame.size.height+30*2+20;
                cell.viewOption3.frame = frame3;
            }
            
            if(cell.viewOption4.frame.origin.y != 500)
            {
                CGRect frame4 = cell.viewOption4.frame;
                //frame4.origin.y = 376;
                CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
                 frame4.size.height = op4;
               // cell.btnOption4.frame = cell.viewOption4.frame;
               // [cell.lblD setFrame:CGRectMake(cell.lblA.frame.origin.x, frame4.origin.y, cell.lblA.frame.size.width, cell.lblA.frame.size.height)];
                frame4.origin.y = cell.txtVQuestion.frame.size.height + cell.viewOption1.frame.size.height + cell.viewOption2.frame.size.height + cell.viewOption3.frame.size.height+30*2+30;
                cell.viewOption4.frame = frame4;
            }
            
            
             CGRect viewback = cell.Vw_Back.frame;
            viewback.size.height = cell.viewOption4.frame.origin.y + cell.viewOption4.frame.size.height + 20;
            cell.Vw_Back.frame = viewback;
            
            cell.scrollVQuestion.contentSize =CGSizeMake(cell.contentView.frame.size.width, viewback.size.height+10);
            
            CGRect frame12 = cell.contentView.frame;
            frame12.size.height = 400;
            cell.contentView.frame = frame12;
        }
        
        
        
        cell.btnOption1.userInteractionEnabled = NO;
        cell.bntOption2.userInteractionEnabled = NO;
        cell.btnOption3.userInteractionEnabled = NO;
        cell.btnOption4.userInteractionEnabled = NO;
     
        
        NSLog(@"GETOptions %@",[GETOptions valueForKey:@"is_selected"]);
        if ([selectedAns1 isEqualToString:@"1"]){
            if ([rightAns1 isEqualToString:@"1"]) {
                if (![marrFlag1 containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                    [marrFlag1 addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
                }
                [self selectedRightOptionColor:cell.lblA ];
                [self unSelectedOptionColor:cell.lblD :cell.lblB :cell.lblC];
                
                cell.btn_flag.userInteractionEnabled = NO;
                
                
                [_collDispQuestionNo reloadData];
                if ([[marrFlag objectAtIndex:indexPath.row] intValue]==1) {
                    [marrFlag replaceObjectAtIndex:indexPath.row withObject:@"2"];
                }
               
             
            }
            else{
                
                cell.btn_flag.userInteractionEnabled = NO;
                [self selectedWrongOptionColor:cell.lblA ];
                
                if ([rightAns2 isEqualToString:@"1"]) {
                    [self selectedRightOptionColor:cell.lblB ];
                    [self unSelectedOptionColor2:cell.lblC :cell.lblD];
                }
                else if ([rightAns3  isEqualToString:@"1"]) {
                    [self selectedRightOptionColor:cell.lblC ];
                    [self unSelectedOptionColor2:cell.lblB :cell.lblD];
                }
                else if ([rightAns4  isEqualToString:@"1"]) {
                    [self selectedRightOptionColor:cell.lblD ];
                    [self unSelectedOptionColor2:cell.lblB :cell.lblC];
                }
                else{
                    
                    [self unSelectedOptionColor:cell.lblD :cell.lblB :cell.lblC];
                }
                
                if ([[marrFlag objectAtIndex:indexPath.row] intValue] != 0) {
                    [marrFlag replaceObjectAtIndex:indexPath.row withObject:@"2"];
                }
                
                [_collDispQuestionNo reloadData];
            }
        }
        else if ([selectedAns2  isEqualToString:@"1"]){
            if ([rightAns2 isEqualToString:@"1"]) {
                if (![marrFlag1 containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                    // modify objectToSearchFor
                    [marrFlag1 addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
                }
                
                    [self selectedRightOptionColor:cell.lblB];
                [self unSelectedOptionColor:cell.lblD :cell.lblA :cell.lblC];
                
                [_collDispQuestionNo reloadData];
                
                if ([[marrFlag objectAtIndex:indexPath.row] intValue]==1) {
                    [marrFlag replaceObjectAtIndex:indexPath.row withObject:@"2"];
                }
                
                
                
                [_collDispQuestionNo reloadData];
            }
            else{
                cell.btn_flag.userInteractionEnabled = NO;
                [self selectedWrongOptionColor:cell.lblB ];
                if ([rightAns1 isEqualToString:@"1"]) {
                    [self selectedRightOptionColor:cell.lblA ];
                    [self unSelectedOptionColor2:cell.lblC :cell.lblD];
                }
                else if ([rightAns3  isEqualToString:@"1"]) {
                    [self selectedRightOptionColor:cell.lblC ];
                    [self unSelectedOptionColor2:cell.lblA :cell.lblD];
                }
                else if ([rightAns4  isEqualToString:@"1"]) {
                    [self selectedRightOptionColor:cell.lblD ];
                    [self unSelectedOptionColor2:cell.lblA :cell.lblC];
                }
                else{
                    
                    [self unSelectedOptionColor:cell.lblD :cell.lblA :cell.lblC];
                }
                
                if ([[marrFlag objectAtIndex:indexPath.row] intValue] != 0) {
                    [marrFlag replaceObjectAtIndex:indexPath.row withObject:@"2"];
                }
                [_collDispQuestionNo reloadData];
            }
            
        }
        else if ([selectedAns3  isEqualToString:@"1"]){
            if ([rightAns3  isEqualToString:@"1"]) {
                if (![marrFlag1 containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                    // modify objectToSearchFor
                    [marrFlag1 addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
                }
                
                [self unSelectedOptionColor:cell.lblA :cell.lblB :cell.lblD];
                    [self selectedRightOptionColor:cell.lblC ];
                
                [_collDispQuestionNo reloadData];
               
                if ([[marrFlag objectAtIndex:indexPath.row] intValue]==1) {
                    [marrFlag replaceObjectAtIndex:indexPath.row withObject:@"2"];
                }
                
                
                [_collDispQuestionNo reloadData];
            }
            else{
                cell.btn_flag.userInteractionEnabled = NO;
                [self selectedWrongOptionColor:cell.lblC ];
                if ([rightAns1 isEqualToString:@"1"]) {
                    [self selectedRightOptionColor:cell.lblA ];
                    [self unSelectedOptionColor2:cell.lblB :cell.lblD];
                }
                else if ([rightAns2 isEqualToString:@"1"]) {
                    [self selectedRightOptionColor:cell.lblB ];
                    [self unSelectedOptionColor2:cell.lblA :cell.lblD];
                }
                else if ([rightAns4  isEqualToString:@"1"]) {
                    [self selectedRightOptionColor:cell.lblD ];
                    [self unSelectedOptionColor2:cell.lblB :cell.lblA];
                }
                else{
                    
                    [self unSelectedOptionColor:cell.lblA :cell.lblB :cell.lblD];
                }
                
                if ([[marrFlag objectAtIndex:indexPath.row] intValue] != 0) {
                    [marrFlag replaceObjectAtIndex:indexPath.row withObject:@"2"];
                }
                [_collDispQuestionNo reloadData];
            }
            
        }
        else if ([selectedAns4  isEqualToString:@"1"]){
            if ([rightAns4  isEqualToString:@"1"]) {
                if (![marrFlag1 containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                    // modify objectToSearchFor
                    [marrFlag1 addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
                }
                
                [self unSelectedOptionColor:cell.lblA :cell.lblB :cell.lblC];
                    [self selectedRightOptionColor:cell.lblD ];
                
                [_collDispQuestionNo reloadData];
                if ([[marrFlag objectAtIndex:indexPath.row] intValue]==1) {
                    [marrFlag replaceObjectAtIndex:indexPath.row withObject:@"2"];
                }
                
                
                [_collDispQuestionNo reloadData];
            }
            else{
                cell.btn_flag.userInteractionEnabled = NO;
                [self selectedWrongOptionColor:cell.lblD ];
                if ([rightAns1 isEqualToString:@"1"]) {
                    [self selectedRightOptionColor:cell.lblA ];
                    [self unSelectedOptionColor2:cell.lblB :cell.lblC];
                }
                else if ([rightAns2 isEqualToString:@"1"]) {
                    [self selectedRightOptionColor:cell.lblB ];
                    [self unSelectedOptionColor2:cell.lblA :cell.lblC];
                }
                else if ([rightAns3  isEqualToString:@"1"]) {
                    [self selectedRightOptionColor:cell.lblC ];
                    [self unSelectedOptionColor2:cell.lblA :cell.lblB];
                }
                else{
                    [self unSelectedOptionColor:cell.lblA :cell.lblB :cell.lblC];
                }
                
                if ([[marrFlag objectAtIndex:indexPath.row] intValue] != 0) {
                    [marrFlag replaceObjectAtIndex:indexPath.row withObject:@"2"];
                }
                [_collDispQuestionNo reloadData];
            }
            
        }
        else{
            
            cell.lblA.textColor = [UIColor darkGrayColor];
            cell.lblB.textColor = [UIColor darkGrayColor];
            cell.lblC.textColor = [UIColor darkGrayColor];
            cell.lblD.textColor = [UIColor darkGrayColor];
            cell.lblA.layer.backgroundColor = [UIColor clearColor].CGColor;
            cell.lblB.layer.backgroundColor = [UIColor clearColor].CGColor;
            cell.lblC.layer.backgroundColor = [UIColor clearColor].CGColor;
            cell.lblD.layer.backgroundColor = [UIColor clearColor].CGColor;
            
            //[self unSelectedOptionColor1:cell.lblA :cell.lblB :cell.lblC :cell.lblD];
            cell.btnOption1.tag = indexPath.row;
            cell.btnOption1.userInteractionEnabled = YES;
            cell.bntOption2.tag = indexPath.row;
            cell.bntOption2.userInteractionEnabled = YES;
            cell.btnOption3.tag = indexPath.row;
            cell.btnOption3.userInteractionEnabled = YES;
            cell.btnOption4.tag = indexPath.row;
            cell.btnOption4.userInteractionEnabled = YES;
         
            [cell.btnOption1 addTarget:self action:@selector(selectedAnswer1:) forControlEvents:UIControlEventTouchUpInside];
            [cell.bntOption2 addTarget:self action:@selector(selectedAnswer2:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnOption3 addTarget:self action:@selector(selectedAnswer3:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnOption4 addTarget:self action:@selector(selectedAnswer4:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        
        //NSLog(@"marrFlag %@",marrFlag);
        
        if (cell.btn_flag.userInteractionEnabled == YES) {
            if ([[marrFlag objectAtIndex:indexPath.row] intValue]==2) {
                cell.btn_flag.userInteractionEnabled = NO;
                cell.btn_flag.selected=NO;
                [cell.btn_flag setImage:[UIImage imageNamed:@"mark-blue"] forState:UIControlStateNormal];
            }
            else{
                //FLAG
                cell.btn_flag.tag=indexPath.row;
                cell.btn_flag.userInteractionEnabled = YES;
                [cell.btn_flag addTarget:self action:@selector(selectedAnswer5:) forControlEvents:UIControlEventTouchUpInside];
                
                
                if ([[marrFlag objectAtIndex:indexPath.row] intValue]==0) {
                    cell.btn_flag.selected=NO;
                    [cell.btn_flag setImage:[UIImage imageNamed:@"mark-grey"] forState:UIControlStateNormal];
                }
                else
                {
                    cell.btn_flag.selected=YES;
                }
                
            }
        }
        else
        {
            if ([[marrFlag objectAtIndex:indexPath.row] intValue]==2) {
                cell.btn_flag.userInteractionEnabled = NO;
                cell.btn_flag.selected=NO;
                [cell.btn_flag setImage:[UIImage imageNamed:@"mark-blue"] forState:UIControlStateNormal];
            }
        }
        
        Lbl_Score.hidden = NO;
        if (marrFlag1.count != 0) {
            Lbl_Score.text = [NSString stringWithFormat:@"Score : %lu %%",marrFlag1.count*100/GETOptions.count];
        }
        else{
            Lbl_Score.text = [NSString stringWithFormat:@"Score : 0 %%"];
        }
        
        
        
        return cell;
    }
    
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height+20;
}
-(void)zoom_img :(id)sender
{
    NSLog(@"Image Zoom is calling");
    
    NSString *strImg=[GETQuestions valueForKey:@"ImageName"][curruntRow];
    
    NSMutableArray *lImages=[[NSMutableArray alloc]init];
    
    
    
    strImg = [strImg stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    NSURL *url = [NSURL URLWithString:strImg];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage* immg = [[UIImage alloc]initWithData:data];
    
    [lImages addObject:immg];
    
    NSLog(@"%@",strImg);
    
    
    
   
   
    NSArray *landscapeImages = [NSArray arrayWithArray:lImages];
    
    
    Class cls = NSClassFromString(@"AFTNormalPagingViewController");
    AFTPagingBaseViewController *vc = [cls new];
    AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
    
    bvc.title = @"Photo";
    bvc.images = landscapeImages;
    [self presentViewController:bvc animated:YES completion:nil];
    
    
    
}
-(void)highlightLetter:(UITapGestureRecognizer*)sender
{
    
    
    
    /*UIImageView *lImages=[[UIImageView alloc]init];
   
        cell.img_question
        
        strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        
        NSURL *url = [NSURL URLWithString:strImage];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage* immg = [[UIImage alloc]initWithData:data];
        
        [lImages addObject:immg];
      */
  
    
    NSArray *landscapeImages = [NSArray arrayWithArray:cell.img_question];
    
    
    Class cls = NSClassFromString(@"AFTNormalPagingViewController");
    AFTPagingBaseViewController *vc = [cls new];
    AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
    
    bvc.title = @"Photo";
    bvc.images = landscapeImages;
    [self presentViewController:bvc animated:YES completion:nil];
    
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
     if(collectionView == self.collDispQuestionNo){
         
         [self moveToQuestion:indexPath];
         
     }else{
         
     
     }
    
}

-(void) reArrangeViewUp : (UIView * ) view {
    CGRect frame = view.frame;
    frame.origin.y -= 150.0f;
    view.frame = frame;
}

-(void) reArrangeViewDown : (UIView * ) view {
    CGRect frame = view.frame;
    frame.origin.y += 150.0f;
    view.frame = frame;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

     if(collectionView == self.collDispQuestionNo){
         
         return CGSizeMake((self.collDispQuestionNo.frame.size.width/10), (self.collDispQuestionNo.frame.size.height));
         
     }
     else{
         
         return CGSizeMake((self.collDispQuestions.frame.size.width),(self.collDispQuestions.frame.size.height));
         
     }
    
}


#pragma mark - UIScrollView Method


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (scrollView == _collDispQuestionNo) {
        
        NSLog(@"_collDispQuestionNo ");
        
    }
    
    else if (scrollView == cell.scrollVQuestion){
        
    }
    else
    {
        NSLog(@"scrollViewDidEndDecelerating");
        
        if (self.lastContentOffset > scrollView.contentOffset.x && self.lastContentOffset > scrollView.contentOffset.y ){
            
            [self moveToPreviousQuestion];
            
            NSLog(@"moveToPreviousQuestion");
            
            
        }
        else if (self.lastContentOffset > scrollView.contentOffset.x){
            
            [self moveToPreviousQuestion];
            
        }else if (self.lastContentOffset < scrollView.contentOffset.x  && self.lastContentOffset > scrollView.contentOffset.y ){
            
            [self moveToNextQuestion];
            
        }
        else if(self.lastContentOffset < scrollView.contentOffset.x) {
            
            [self moveToNextQuestion];

        }
    }
    //NSLog(@"lastContentOffset : %f",_lastContentOffset);
    //NSLog(@"scrollView.contentOffset.x  : %f",scrollView.contentOffset.x);
    self.lastContentOffset = scrollView.contentOffset.x;
    
   
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{

    NSLog(@"scrollViewDidEndScrollingAnimation");
    
    self.lastContentOffset = scrollView.contentOffset.x;
    NSLog(@"lastContentOffset : %f",_lastContentOffset);


}

#pragma mark - Change Other Method
/*
-(void)changeSelection:(int )index{
    
   
    if (![[[ShareData.marrmcqquestion objectAtIndex:index] valueForKey:@"selectedAns"] isEqual:@"0"]) {
        
        int  SelectedAnswer = [[[ShareData.marrmcqquestion objectAtIndex:index] valueForKey:@"selectedAns"] intValue];
        NSLog(@"selected Answer : %d",SelectedAnswer);
        
        switch (SelectedAnswer) {
            case 1:
                
                [self selectedOptionColor:cell.lblA ];
                [self unSelectedOptionColor:cell.lblB :cell.lblC  :cell.lblD];
                
                break;
                
            case 2:
                
                [self selectedOptionColor:cell.lblB];
                [self unSelectedOptionColor:cell.lblA :cell.lblC  :cell.lblD ];
                break;
                
            case 3:
                
                [self selectedOptionColor:cell.lblC ];
                [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblD ];
                break;
                
            case 4:
                
                
                [self selectedOptionColor:cell.lblD ];
                [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblC ];
                
                break;
                
            default:
                break;
                
        }
        
    }
    else{
        
        cell.lblA.layer.backgroundColor = [UIColor clearColor].CGColor;
        cell.lblB.layer.backgroundColor = [UIColor clearColor].CGColor;
        cell.lblC.layer.backgroundColor = [UIColor clearColor].CGColor;
        cell.lblD.layer.backgroundColor = [UIColor clearColor].CGColor;
        
        
    }

}

*/

#pragma mark - Convert HTML Formated String
-(NSAttributedString *)convertIntoAttributadStr:(NSString *)str{
    
    NSString *htmlString = str;
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    
    
    return attributedString;
}



#pragma mark - Change Height Of Textview

- (void)changeTextViewHeight:(UITextView *)textView :(NSLayoutConstraint*)heightConstraint
{
    NSLog(@"changeTextViewHeight");
   
    
    CGSize constraint = CGSizeMake(textView.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [textView.text boundingRectWithSize:constraint
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:textView.font}
                                                     context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    heightConstraint.constant = size.height + 20;
    
    NSLog(@"height : %f",size.height);
    
    
    
}

#pragma mark - change Webview height

- (void)changeheightOfWebview:(UIWebView *)aWebView :(NSLayoutConstraint*)heightConstraint{
    
    NSString *str = [aWebView stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
    CGFloat height = str.floatValue;
    heightConstraint.constant = height + 15.0;
    NSLog(@"height: %f", height);
}


#pragma mark - Webview Delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    
    if (webView.tag == 1) {
        
        if ([strQuestion isEqualToString:@"Web"]) {
            [self changeheightOfWebview:cell.webVQuestion :cell.viewQuestionHeight];
        }
    }
    
    if (webView.tag == 2) {
        
        if ([strOption1 isEqualToString:@"Web"]){
            [self changeheightOfWebview:cell.webVOption1 :cell.viewOption1Height];
        }
    }
    
    if (webView.tag == 3) {
        
        if ([strOption2 isEqualToString:@"Web"]){
            [self changeheightOfWebview:cell.webVOption2 :cell.viewOption2Height];
        }
    }
    
    if (webView.tag == 4) {
        
        if ([strOption3 isEqualToString:@"Web"]){
            [self changeheightOfWebview:cell.webVOption3 :cell.viewOption3Height];
        }
    }
    
    if (webView.tag == 5) {
        
        if ([strOption4 isEqualToString:@"Web"]){
            [self changeheightOfWebview:cell.webVOption4 :cell.viewOption4Height];
        }
    }
}

#pragma mark - Check Question and Option String

- (NSString*)displayWebviewORTextView:(NSString *)Str Webview:(UIWebView*)Webview TextView:(UITextView*)TextView BGView:(NSLayoutConstraint*)BGViewHeightConstant {
    
    if ([Str containsString:@"<img"]|| [Str containsString:@"<math"] || [Str containsString:@"<table"] ) {
        
        NSLog(@"string contains tag!");
        if ([Str containsString:@"src"]) {
            Str = [Str stringByReplacingOccurrencesOfString:@"src=\"/" withString:@"src=\"http://staff.parshvaa.com/"];
            NSLog(@"apdateString %@",Str);
        }
        TextView.hidden =true;
        Webview.hidden =false;
        [Webview loadHTMLString:Str baseURL:nil];
        
        return @"Web";
        
    } else {
        
        NSLog(@"string does not contain tags");
        Webview.hidden =true;
        TextView.hidden =false;
        TextView.attributedText = [self convertIntoAttributadStr:[NSString stringWithFormat:@"%@",Str]];
        [self changeTextViewHeight:TextView :BGViewHeightConstant];
        return @"Text";
    }
}
#pragma mark - Action Method

- (IBAction)btnBack:(id)sender {
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Are you sure you want to Leave the test?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * yes = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    UIAlertAction * no = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:no];
    [alert addAction:yes];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)bntPrevious:(id)sender {
    
    NSLog(@"btnPrevious tap");
    
    [self moveToPreviousQuestion];
    
}

- (IBAction)btnNext:(id)sender {
    
    NSLog(@"btnNext tap");
    [self moveToNextQuestion];
    
}

- (IBAction)SaveandExitButtonPressed:(id)sender {
    GetView.hidden = YES;
}

- (IBAction)ExitButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)ExitQuizButtonPressed:(id)sender {
    GetView.hidden = NO;
}

#pragma mark - Move NEXT_PREVIOUS Method

-(void)moveToNextQuestion{
    
    NSLog(@"moveToNextQuestion");
    curruntRow++;
    
    if(curruntRow == GETQuestions.count - 1){
        
        _btnNext.enabled = false;
        _btnPrevious.enabled = true;
        
    }else{
        
        if(curruntRow > 0 ){
            _btnPrevious.enabled = true;
        }
    }
    
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:curruntRow inSection:0];
    
    [self.collDispQuestions scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self.collDispQuestionNo scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    [self changeQuestionNo:curruntRow+1];
    
    NSLog(@"curruntRow %d",curruntRow);
    
    [self.collDispQuestionNo reloadData];    

}

-(void)moveToPreviousQuestion{
    
    NSLog(@"moveToPreviousQuestion");
    curruntRow--;
    
    if(curruntRow == 0){
        _btnNext.enabled=true;
        _btnPrevious.enabled = false;
        
    }else{
        
        if(curruntRow < GETQuestions.count){
            
            _btnNext.enabled=true;
            
        }
    }
    
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:curruntRow inSection:0];
    
    [self.collDispQuestions scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self.collDispQuestionNo scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    [self changeQuestionNo:curruntRow+1];
    NSLog(@"curruntRow %d",curruntRow);
    
    [self.collDispQuestionNo reloadData];

}

-(void)moveToQuestion:(NSIndexPath*)indexpath{

    NSLog(@"Indexpath : %ld",(long)indexpath.row);
   
    curruntRow = (int)indexpath.row;
    
    if (curruntRow > 0) {
        
        _btnPrevious.enabled = true;
        
        if(curruntRow == GETQuestions.count - 1){
            
            _btnNext.enabled = false;
            _btnPrevious.enabled = true;
            
        }else{
            
            if(curruntRow > 0 ){
                
                _btnPrevious.enabled = true;
            }
        }
        
    }else{
    
        _btnPrevious.enabled = false;
    }
    
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:curruntRow inSection:0];
    
    [self.collDispQuestions scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    [self.collDispQuestionNo scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
    
    [self changeQuestionNo:curruntRow+1];

    [self.collDispQuestionNo reloadData];
    //[self.collDispQuestions reloadData];
}

- (void)changeQuestionNo:(int)questionNo{
    
    int j=0;
    for (int i=0; i<arrAttamt.count; i++) {
        if ([[arrAttamt objectAtIndex:i] intValue]==1) {
            j++;
        }
    }
    // _lbl_attemp.text=[NSString stringWithFormat:@"%02d",j];
    
    NSMutableAttributedString *CreateAcc;
    NSRange linkRange2;
    CreateAcc = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attempt %02d / %lu",j,arrAttamt.count] attributes:nil];
    NSString *AccString = [NSString stringWithFormat:@"Attempt %02d / %lu",j,arrAttamt.count];
    linkRange2 = [AccString rangeOfString:[NSString stringWithFormat:@"%02d", j]];
    
    
    NSDictionary *linkAttributes2 = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:0.05 green:0.4 blue:0.65 alpha:1.0]};
    [CreateAcc setAttributes:linkAttributes2 range:linkRange2];
    
    // Assign attributedText to UILabel
    _lblDispQuestionNo.attributedText = CreateAcc;
    
}

#pragma mark - ANSWER Select_Unselect Method

-(void)selectedOptionColor:(UILabel*)label{
    label.textColor = [UIColor whiteColor];
    label.layer.backgroundColor = [UIColor greenColor].CGColor;
    label.layer.cornerRadius = label.frame.size.height/2;
    
   [self.collDispQuestions reloadData];
}

-(void)selectedRightOptionColor:(UILabel*)label{
    label.textColor = [UIColor whiteColor];
    label.layer.backgroundColor = [UIColor greenColor].CGColor;
    label.layer.cornerRadius = label.frame.size.height/2;
}

-(void)selectedRightOptionColor1:(UILabel*)label : (NSInteger)getnew : (NSInteger)getIndex
{
    
    if ([[arrAttamt objectAtIndex:curruntRow] intValue]==0){
        [arrAttamt replaceObjectAtIndex:curruntRow withObject:@"1"];
    }
    
    NSLog(@"arrAttamt %lu",arrAttamt.count);
    
    int j=00;
    for (int i=0; i<arrAttamt.count; i++) {
        if ([[arrAttamt objectAtIndex:i] intValue]==1) {
            j++;
        }
    }
    // _lbl_attemp.text=[NSString stringWithFormat:@"%02d",j];
    
    NSMutableAttributedString *CreateAcc;
    NSRange linkRange2;
    CreateAcc = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attempt %02d / %lu",j,arrAttamt.count] attributes:nil];
    NSString *AccString = [NSString stringWithFormat:@"Attempt %02d / %lu",j,arrAttamt.count];
    linkRange2 = [AccString rangeOfString:[NSString stringWithFormat:@"%02d", j]];
    
    
    NSDictionary *linkAttributes2 = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:0.05 green:0.4 blue:0.65 alpha:1.0]};
    [CreateAcc setAttributes:linkAttributes2 range:linkRange2];
    
    // Assign attributedText to UILabel
    _lblDispQuestionNo.attributedText = CreateAcc;
    
    
    label.textColor = [UIColor whiteColor];
    label.layer.backgroundColor = [UIColor greenColor].CGColor;
    label.layer.cornerRadius = label.frame.size.height/2;
    
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    NSDictionary *oldDict = (NSDictionary *)[GETQuestions valueForKeyPath:@"options"][getnew][getIndex];
    [newDict addEntriesFromDictionary:oldDict];
    [newDict setObject:@"1" forKey:@"is_selected"];
    
    NSMutableArray *getarr = [[NSMutableArray alloc] init];
    for (NSString *str in GETOptions[getnew]) {
        [getarr addObject:str];
    }
    
    [getarr replaceObjectAtIndex:getIndex withObject:newDict];
    
    NSMutableArray *get = [[NSMutableArray alloc] init];
    for (NSString *str in GETOptions) {
        [get addObject:str];
    }
    [get replaceObjectAtIndex:getnew withObject:getarr];
    
    GETOptions = [[NSMutableArray alloc] init];
    for (NSString *str in get) {
        [GETOptions addObject:str];
    }

    [_collDispQuestions reloadData];
}

-(void)selectedWrongOptionColor:(UILabel*)label{
    label.textColor = [UIColor whiteColor];
    label.layer.backgroundColor = [UIColor redColor].CGColor;
    label.layer.cornerRadius = label.frame.size.height/2;
}

-(void)unSelectedOptionColor:(UILabel*)label1 :(UILabel*)label2 :(UILabel*)label3{
    label1.textColor = [UIColor darkGrayColor];
    label2.textColor = [UIColor darkGrayColor];
    label3.textColor = [UIColor darkGrayColor];
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
    label3.layer.backgroundColor = [UIColor clearColor].CGColor;
}

-(void)unSelectedOptionColor2:(UILabel*)label1 :(UILabel*)label2{
    label1.textColor = [UIColor darkGrayColor];
    label2.textColor = [UIColor darkGrayColor];
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
}

-(void)unSelectedOptionColor1:(UILabel*)label1 :(UILabel*)label2 :(UILabel*)label3 :(UILabel*)label4{
    label1.textColor = [UIColor darkGrayColor];
    label2.textColor = [UIColor darkGrayColor];
    label3.textColor = [UIColor darkGrayColor];
    label4.textColor = [UIColor darkGrayColor];
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
    label3.layer.backgroundColor = [UIColor clearColor].CGColor;
    label4.layer.backgroundColor = [UIColor clearColor].CGColor;
}

-(void)selectedAnswer1:(UIButton *)sender{
    //[self selectedOptionColor:cell.lblA ];
    [self selectedRightOptionColor1:cell.lblA :[sender tag] :0];
    [self unSelectedOptionColor:cell.lblB :cell.lblC  :cell.lblD ];
}
-(void)selectedAnswer2:(UIButton *)sender{
    //[self selectedOptionColor:cell.lblB];
    [self selectedRightOptionColor1:cell.lblB :[sender tag] :1];
    [self unSelectedOptionColor:cell.lblA :cell.lblC  :cell.lblD ];
}

-(void)selectedAnswer3:(UIButton *)sender{
    //[self selectedOptionColor:cell.lblC];
    [self selectedRightOptionColor1:cell.lblC :[sender tag] :2];
    [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblD ];
}

-(void)selectedAnswer4:(UIButton *)sender{
    //[self selectedOptionColor:cell.lblD ];
    [self selectedRightOptionColor1:cell.lblD :[sender tag] :3];
    [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblC];
}
-(void)selectedAnswer5:(UIButton *)sender{
    
    UIButton *button = (UIButton *)sender;
        
    if ([[marrFlag objectAtIndex:sender.tag] intValue]==0) {
        [marrFlag replaceObjectAtIndex:sender.tag withObject:@"1"];
    }
    else
    {
        [marrFlag replaceObjectAtIndex:sender.tag withObject:@"0"];
    }
    
        
    NSLog(@"---%@",marrFlag);
    if (button.selected == YES) {
        button.selected = NO;
    }
    else{
        button.selected = YES;
    }
    
    [_collDispQuestionNo reloadData];
    [_collDispQuestions reloadData];
    
    
    
}






- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark - SWRevealViewController Delegate

//revealController didMoveToPosition
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft)
    {
        revealController.frontViewController.view.alpha = 1;
        [self.revealViewController tapGestureRecognizer];
        self.view.userInteractionEnabled = YES;
    }
    else
    {
        revealController.frontViewController.view.alpha = 0.5;
        
        self.view.userInteractionEnabled = NO;
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
}


@end
