//
//  CVCDispQuestions.h
//  McqApp
//
//  Created by My Mac on 3/9/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVCDispQuestions : UICollectionViewCell
{
   
}
@property (weak, nonatomic) IBOutlet UIView *viewDispQuestion;
//@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (strong, nonatomic) IBOutlet UIView *Vw_Back;
@property (strong, nonatomic) IBOutlet UIView *Vw_Header;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_PageNumber;
@property (strong, nonatomic) IBOutlet UIButton *btn_Flag;


@property (weak, nonatomic) IBOutlet UITextView *txtVQuestion;
@property (weak, nonatomic) IBOutlet UIImageView *img_question;

@property (weak, nonatomic) IBOutlet UIWebView *webVQuestion;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webvQuestionoHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewQuestionHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVQuestionHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollVQuestion;



@property (weak, nonatomic) IBOutlet UIView *viewOption1;
@property (weak, nonatomic) IBOutlet UIView *viewOption2;
@property (weak, nonatomic) IBOutlet UIView *viewOption3;
@property (weak, nonatomic) IBOutlet UIView *viewOption4;


@property (weak, nonatomic) IBOutlet UILabel *lblA;
@property (weak, nonatomic) IBOutlet UILabel *lblB;
@property (weak, nonatomic) IBOutlet UILabel *lblC;
@property (weak, nonatomic) IBOutlet UILabel *lblD;


@property (strong, nonatomic) IBOutlet UILabel *txtVOption1;
@property (strong, nonatomic) IBOutlet UILabel *txtVOption2;
@property (strong, nonatomic) IBOutlet UILabel *txtVOption3;
@property (strong, nonatomic) IBOutlet UILabel *txtVOption4;

@property (weak, nonatomic) IBOutlet UIWebView *webVOption1;
@property (weak, nonatomic) IBOutlet UIWebView *webVOption2;
@property (weak, nonatomic) IBOutlet UIWebView *webVOption3;
@property (weak, nonatomic) IBOutlet UIWebView *webVOption4;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewOption1Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewOption2Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewOption3Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewOption4Height;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVOption1Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVOption2Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVOption3Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVOption4Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVOption1Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVOption2Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVOption3Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVOption4Height;




@property (weak, nonatomic) IBOutlet UIButton *btnOption1;
@property (weak, nonatomic) IBOutlet UIButton *bntOption2;
@property (weak, nonatomic) IBOutlet UIButton *btnOption3;
@property (weak, nonatomic) IBOutlet UIButton *btnOption4;
@property (strong, nonatomic) IBOutlet UIButton *btn_flag;
@property (strong, nonatomic) IBOutlet UIButton *btn_img_zoom;
@property (weak, nonatomic) IBOutlet UIButton *btn_zoom;



- (IBAction)btnOption1:(id)sender;
- (IBAction)btnOption2:(id)sender;
- (IBAction)btnOption3:(id)sender;
- (IBAction)btnOption4:(id)sender;
- (IBAction)btn_FLAG_A:(id)sender;


//////////////////////////////////
//NEW TEST////////
//////////////////////////////////

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_vw;
@property (weak, nonatomic) IBOutlet UIView *vw_header;
@property (weak, nonatomic) IBOutlet UIView *vw_1;
@property (weak, nonatomic) IBOutlet UIView *vw_A;
@property (weak, nonatomic) IBOutlet UIView *vw_B;
@property (weak, nonatomic) IBOutlet UIView *vw_C;
@property (weak, nonatomic) IBOutlet UIView *vw_D;

@property (weak, nonatomic) IBOutlet UILabel *lbl_que;
@property (weak, nonatomic) IBOutlet UILabel *lbl_A;
@property (weak, nonatomic) IBOutlet UILabel *lbl_B;
@property (weak, nonatomic) IBOutlet UILabel *lbl_C;
@property (weak, nonatomic) IBOutlet UILabel *lbl_D;

@property (weak, nonatomic) IBOutlet UILabel *lbl_A_opt;
@property (weak, nonatomic) IBOutlet UILabel *lbl_B_opt;
@property (weak, nonatomic) IBOutlet UILabel *lbl_C_opt;
@property (weak, nonatomic) IBOutlet UILabel *lbl_D_opt;

@property (weak, nonatomic) IBOutlet UILabel *lbl_que_no;
@property (weak, nonatomic) IBOutlet UIButton *btn_mark;

@property (weak, nonatomic) IBOutlet UIView *vw_main;
@property (weak, nonatomic) IBOutlet UIView *vw_que_opt;












@end
