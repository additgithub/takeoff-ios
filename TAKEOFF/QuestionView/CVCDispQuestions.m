//
//  CVCDispQuestions.m
//  McqApp
//
//  Created by My Mac on 3/9/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#import "CVCDispQuestions.h"



@implementation CVCDispQuestions



-(void)awakeFromNib{

    [super awakeFromNib];
    
    _Vw_Back.layer.borderWidth = 1.0;
    _Vw_Back.layer.borderColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1].CGColor;
    
    _txtVOption1.layer.borderWidth = 1.0;
    _txtVOption1.layer.borderColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1].CGColor;
    
    _txtVOption2.layer.borderWidth = 1.0;
    _txtVOption2.layer.borderColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1].CGColor;
    
    _txtVOption3.layer.borderWidth = 1.0;
    _txtVOption3.layer.borderColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1].CGColor;
    
    _txtVOption4.layer.borderWidth = 1.0;
    _txtVOption4.layer.borderColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1].CGColor;

   
    /*[self setBGColor:_webVQuestion];
    [self setBGColor:_webVOption1];
    [self setBGColor:_webVOption2];
    [self setBGColor:_webVOption3];
    [self setBGColor:_webVOption4];
    
    _lblA.layer.cornerRadius=_lblA.frame.size.height/2;
    _lblB.layer.cornerRadius=_lblB.frame.size.height/2;
    _lblC.layer.cornerRadius=_lblC.frame.size.height/2;
    _lblD.layer.cornerRadius=_lblD.frame.size.height/2;
*/
    
    ///NewTest
    
    _lbl_que.layer.borderWidth = 1.0;
    _lbl_que.layer.borderColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1].CGColor;
    
    _lbl_A_opt.layer.borderWidth = 1.0;
    _lbl_A_opt.layer.borderColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1].CGColor;
   
    _lbl_B_opt.layer.borderWidth = 1.0;
    _lbl_B_opt.layer.borderColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1].CGColor;
   
    _lbl_C_opt.layer.borderWidth = 1.0;
    _lbl_C_opt.layer.borderColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1].CGColor;
    
    _lbl_D_opt.layer.borderWidth = 1.0;
    _lbl_D_opt.layer.borderColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1].CGColor;
    
    //_txtVOption1.textContainerInset = UIEdgeInsetsMake(0, 20, 0, 20);
    //@property(nonatomic, assign) UIEdgeInsets textContainerInset NS_AVAILABLE_IOS(7_0);
    
    
   
}




-(void)setBGColor:(UIWebView*)Webview{
    
    //Webview.delegate = self;
    //Webview.scrollView.scrollEnabled = NO;
    //Webview.scrollView.bounces = NO;
    [Webview setBackgroundColor:[UIColor clearColor]];
    [Webview setOpaque:NO];
    
}
/*
- (IBAction)btnOption1:(id)sender {
 
    [self selectedOptionColor:_lblA ];
    [self unSelectedOptionColor:_lblB :_lblC  :_lblD  ];
    
}

- (IBAction)btnOption2:(id)sender {
    
    [self selectedOptionColor:_lblB];
   [self unSelectedOptionColor:_lblA :_lblC  :_lblD ];

}

- (IBAction)btnOption3:(id)sender {
    
    [self selectedOptionColor:_lblC];
    [self unSelectedOptionColor:_lblA :_lblB  :_lblD ];

}

- (IBAction)btnOption4:(id)sender {
    
    [self selectedOptionColor:_lblD ];
    [self unSelectedOptionColor:_lblA :_lblB  :_lblC];
    
}
*/
- (IBAction)btn_FLAG_A:(id)sender {
}


-(void)selectedOptionColor:(UILabel*)label{

    label.textColor = [UIColor whiteColor];
    label.layer.backgroundColor = [UIColor greenColor].CGColor;
    label.layer.cornerRadius = label.frame.size.height/2;
    
    /*
    textView.layer.borderWidth = 1;
    textView.layer.borderColor = [UIColor redColor].CGColor;
    textView.layer.cornerRadius = 2 ;*/
    
}

-(void)unSelectedOptionColor:(UILabel*)label1 :(UILabel*)label2 :(UILabel*)label3  {
    
    label1.textColor = [UIColor darkGrayColor];
    label2.textColor = [UIColor darkGrayColor];
    label3.textColor = [UIColor darkGrayColor];
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
    label3.layer.backgroundColor = [UIColor clearColor].CGColor;
}
@end
