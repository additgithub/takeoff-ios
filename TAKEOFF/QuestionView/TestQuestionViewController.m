////
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "TestQuestionViewController.h"

@interface TestQuestionViewController ()
{
    CGFloat op1;
    CGFloat op2;
    CGFloat op3;
    CGFloat op4;
}
@property(nonatomic, assign) UIEdgeInsets textContainerInset NS_AVAILABLE_IOS(7_0);

@end



@implementation TestQuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gotoFlag=0;
    
    
    self.title = @"Piloto Privado Avión";
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:46/255.0 green:151/255.0 blue:230/255.0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Semibold" size:17.0]}];
    
    _lblUserName.text = @"Username";
    
    curruntRow = 0;
    
    
    
    _btnPrevious.enabled = false;
    
    cell.scrollVQuestion.delegate = self;
    
    marrFlag1=[[NSMutableArray alloc]init];
    marrFlag2 = [[NSMutableArray alloc] init];
    
    ExitView.layer.cornerRadius = 5.0;
    ExitView.layer.masksToBounds = true;
    
    //The setup code (in viewDidLoad in your view controller)
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [GetView addGestureRecognizer:singleFingerTap];
    
    if (AddNew.count <= 0) {
        //Array Allocation
        AddNew = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    if (AddNew2.count <= 0) {
        //Array Allocation
        AddNew2 = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    [self CallMyMethod];
}


-(void)CallMyMethod
{
    [[AppDelegate sharedAppDelegate] showProgress];
    
    NSString *UserID = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"UserID"];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    if (_GETSTUDY == YES) {
        [AddPost setValue:_GetTestHeaderId forKey:@"test_header_id"];
        [AddPost setValue:UserID forKey:@"user_id"];
    }
    else{
        [AddPost setValue:_GetChapterID forKey:@"no_of_questions"];
        [AddPost setValue:UserID forKey:@"user_id"];
    }
    
    NSLog(@"%@%@",JsonUrlConstant,GETTESTDETAIL);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpQuestion  = [[HttpWrapper alloc] init];
        httpQuestion.delegate=self;
        httpQuestion.getbool=NO;
        if (_GETSTUDY == YES) {
            [httpQuestion requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,GETTESTDETAIL] param:[AddPost copy]];
        }
        else{
            [httpQuestion requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,GETTestQuestionAPI] param:[AddPost copy]];
        }
    });
    
}

-(void)SubmitTestDetails : (NSString *)exit
{
    [[AppDelegate sharedAppDelegate] showProgress];
    
    NSString *UserID = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"UserID"];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    //NSString *GetJsonString = [self JsonFormate];
    //NSLog(@"GetJsonString %@",GetJsonString);
    
    //NSArray *getarr =[self JsonFormate];

    
    //QA Array
    NSMutableArray *QA = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<marrFlag1.count; i++) {
        NSMutableDictionary *Pictures = [[NSMutableDictionary alloc] init];
        [Pictures setObject:marrFlag1[i] forKey:@"AnswerdQueOptionID"];
        [Pictures setObject:marrFlag[i] forKey:@"MarkedQueOptionID"];
        [Pictures setObject:marrFlag2[i] forKey:@"TestDTLID"];
        
        [QA addObject:Pictures];
        
    }
    
    NSString *TestDTLID=[NSString stringWithFormat:@"%@",[GETQuestions valueForKey:@"QuestionID"][curruntRow]];
    
    
    
    NSLog(@"Stage_1 %@",QA);
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:QA
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", jsonData);
    
    [AddPost setValue:[GETQuestions valueForKey:@"TestHDRID"][0] forKey:@"test_header_id"];
    [AddPost setValue:UserID forKey:@"user_id"];
    [AddPost setValue:jsonString forKey:@"detail_array"];
    [AddPost setValue:exit forKey:@"is_exit"];
    [AddPost setValue:TestDTLID forKey:@"last_visited_question"];
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@%@",JsonUrlConstant,SubmitTest]);
    NSLog(@"ADDPOST : %@",AddPost);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpSubmit  = [[HttpWrapper alloc] init];
        httpSubmit.delegate=self;
        httpSubmit.getbool=NO;
        [httpSubmit requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,SubmitTest] param:[AddPost copy]];
    });
    
}


-(NSArray *)JsonFormate
{
    //[{"AnswerdQueOptionID":"2","MarkedQueOptionID":"1","TestDTLID":"843"}]
    
    
    //QA Array
    NSMutableArray *QA = [[NSMutableArray alloc] init];
        
    for (int i = 0; i<marrFlag1.count; i++) {
        NSMutableDictionary *Pictures = [[NSMutableDictionary alloc] init];
        [Pictures setObject:marrFlag1[i] forKey:@"AnswerdQueOptionID"];
        [Pictures setObject:marrFlag[i] forKey:@"MarkedQueOptionID"];
        [Pictures setObject:marrFlag2[i] forKey:@"TestDTLID"];
        
        [QA addObject:Pictures];
        
    }
    
    NSLog(@"Stage_1 %@",QA);
    
   /* NSError *error = nil;
    NSData *jsondata = [NSJSONSerialization dataWithJSONObject:QA options:NSJSONWritingPrettyPrinted error:&error];
    NSString *Stage_1 = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
    NSLog(@"%@",Stage_1);*/
    
    return QA;
}

#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    NSLog(@"dict:%@",dicsResponse);
    
    [[AppDelegate sharedAppDelegate] hideProgress];
    if ([[dicsResponse valueForKey:@"status"] boolValue] == true) {
        
        if(wrapper == httpQuestion && httpQuestion != nil)
        {
            NSString *LastVID=[NSString stringWithFormat:@"%@",[dicsResponse valueForKeyPath:@"test_header_data.LastVisitedQuestion"]];
            int TempID=0;
            
            GETQuestions = [dicsResponse valueForKey:@"data"];
            GETOptions = [[NSMutableArray alloc] init];

            GETOptions = [GETQuestions valueForKey:@"options"];
            
            marrFlag=[[NSMutableArray alloc] init];
            arrAttamt=[[NSMutableArray alloc]init];
            
            for (int i = 0; i<GETQuestions.count; i++) {
                [marrFlag addObject:[GETQuestions valueForKey:@"MarkedQueOptionID"][i]];
                [arrAttamt addObject:[GETQuestions valueForKey:@"MarkedQueOptionID"][i]];
                
                int QID=[[GETQuestions valueForKey:@"QuestionID"][i] intValue];
                
                if (QID==[LastVID intValue]) {
                    TempID=i;
                }
                
                
            }
            NSLog(@"marrflag %@",marrFlag);
            
            
            
            // Assign attributedText to UILabel
            _lblDispQuestionNo.text=[NSString stringWithFormat:@"Attampt 00/%lu",(unsigned long)marrFlag.count];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:TempID inSection:0];
            
             //[self changeQuestionNo:TempID];
            
            [self.collDispQuestionNo reloadData];
            [self.collDispQuestions reloadData];
            
           [self moveToQuestion:indexPath];
            
        }
        else if (wrapper == httpSubmit && httpSubmit != nil)
        {
            NSLog(@"dict:%@",dicsResponse);
            
            [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
            
            
            if (gotoFlag) {
                RatingViewController  * next = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingViewController"];
                
                next.gethaderid = [GETQuestions valueForKey:@"TestHDRID"][0];
                [self.navigationController pushViewController:next animated:YES];
            }
            else
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            
        }
        else{
            
            if ([[dicsResponse valueForKey:@"status"] integerValue] == 1) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        
    }
    else
    {
        [[AppDelegate sharedAppDelegate] hideProgress];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
    }
}

//fetchDataFail
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
    [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:FAIL_DATA_MSG];
    NSLog(@"Fetch Data Fail Error:%@",error);
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    GetView.hidden = YES;
    //Do stuff here...
}

-(void)viewWillLayoutSubviews{
    
    self.btnSubmit.layer.borderWidth = 1.0;
    self.btnSubmit.layer.cornerRadius = 5 ;
    self.btnSubmit.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
}


#pragma mark - UICollectionView Method


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return GETQuestions.count;
}

-(void) AnsCall
{
    //**********************************************************
    if(cell.viewOption1.frame.origin.y != 500)
    {
        CGRect frame1 = cell.viewOption1.frame;
        //frame1.origin.y = 202;
        CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
        frame1.origin.y = cell.txtVQuestion.frame.size.height+30*2;
        //                frame1.origin.y = cell.txtVQuestion.frame.size.height +op123+20*2;
        cell.viewOption1.frame = frame1;
        NSString * op123kkkk = [NSString stringWithFormat:@"%f",op123];
        NSLog(@"op123kkkk = %@",op123kkkk);
        
    }
    
    if(cell.viewOption2.frame.origin.y != 500)
    {
        CGRect frame2 = cell.viewOption2.frame;
        //frame2.origin.y = 260;
        CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
        frame2.origin.y = cell.txtVQuestion.frame.size.height + cell.viewOption1.frame.size.height+30*2+10;
        cell.viewOption2.frame = frame2;
    }
    
    if(cell.viewOption3.frame.origin.y != 500)
    {
        CGRect frame3 = cell.viewOption3.frame;
        //frame3.origin.y = 317;
        CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
        frame3.origin.y = cell.txtVQuestion.frame.size.height + cell.viewOption1.frame.size.height +cell.viewOption2.frame.size.height+30*2+20;
        cell.viewOption3.frame = frame3;
    }
    
    if(cell.viewOption4.frame.origin.y != 500)
    {
        CGRect frame4 = cell.viewOption4.frame;
        //frame4.origin.y = 376;
        CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
        frame4.origin.y = cell.txtVQuestion.frame.size.height + cell.viewOption1.frame.size.height + cell.viewOption2.frame.size.height + cell.viewOption3.frame.size.height+30*2+30;
        cell.viewOption4.frame = frame4;
    }
    
    //[UICollectionView reloadItemsAtIndexPaths:@[indexPath]];
    
    //        cell.scrollVQuestion.contentSize =CGSizeMake(cell.contentView.frame.size.width, 400);
    //        CGRect frame12 = cell.contentView.frame;
    //        frame12.size.height = 400;
    //        cell.contentView.frame = frame12;
    
    //[collectionView reloadItemsAtIndexPaths:@[indexPath]];
    
    [cell.scrollVQuestion setNeedsDisplay];
    //**********************************************************
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    if(collectionView == self.collDispQuestionNo)
    {
        
        CVCQuestionNo *cellNo = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellQuestionNo" forIndexPath:indexPath];
        
        
        cellNo.lblQuestionNo.text = [NSString stringWithFormat:@"%ld",indexPath.row + 1];
        [cellNo.lblSelection setHidden:true];
        
        if(indexPath.row == curruntRow){
            [cellNo.lblSelection setHidden:false];
            
        }
        
        if ([[arrAttamt objectAtIndex:indexPath.row] intValue]==0) {
            cellNo.lbl_selected.hidden = YES;
        }
        else
        {
            cellNo.lbl_selected.hidden = NO;
        }
        
        
        
        cellNo.btn_flag1.layer.cornerRadius = cellNo.btn_flag1.frame.size.height/2;
        cellNo.btn_flag1.layer.masksToBounds = YES;
        cellNo.btn_flag1.tag=5;
        
        if ([[marrFlag objectAtIndex:indexPath.row] intValue]==0) {
            cellNo.btn_flag1.hidden = YES;
        }
        else
        {
            cellNo.btn_flag1.hidden = NO;
            if ([[marrFlag objectAtIndex:indexPath.row] intValue]==2) {
                cellNo.btn_flag1.selected = YES;
            }
            else{
                cellNo.btn_flag1.selected = NO;
            }
        }
        return cellNo;
    }
    else
    {
        
        
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        
        
        cell.scrollVQuestion.layer.borderWidth = 1.0;
        cell.scrollVQuestion.layer.borderColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1].CGColor;
        cell.viewQuestionHeight.constant = 60.0;
        cell.viewOption1Height.constant = 60.0;
        cell.viewOption2Height.constant = 60.0;
        cell.viewOption3Height.constant = 60.0;
        cell.viewOption4Height.constant = 60.0;
        

        
        cell.Lbl_PageNumber.text=[NSString stringWithFormat:@"%ld.",indexPath.row+1];
        
        cell.btn_flag.userInteractionEnabled = YES;
        
        cell.txtVQuestion.text = [GETQuestions valueForKey:@"QuestionName"][indexPath.row];
//        cell.txtVQuestion.scrollEnabled = false;
//        CGFloat QHeight=[self getLabelHeight:cell.txtVQuestion];
//        [cell.txtVQuestion sizeToFit];
//        CGRect QH = cell.txtVQuestion.frame;
//        QH.size.height = QH.size.height;
//        cell.txtVQuestion.frame = QH;
        
        [cell.txtVQuestion  setScrollEnabled:NO];
        CGFloat fixedWidth = cell.txtVQuestion.frame.size.width;
        CGSize newSize = [cell.txtVQuestion sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = cell.txtVQuestion.frame;
        newFrame.size = CGSizeMake(fmaxf(newFrame.size.width, fixedWidth), newSize.height);
        cell.txtVQuestion.frame = newFrame;
        [cell.txtVQuestion layoutIfNeeded];
        [cell layoutIfNeeded];
        
        //[self AnsCall];
        
        
        
        
        NSString *imgURL =  [GETQuestions valueForKey:@"ImageName"][indexPath.row];
        cell.txtVQuestion.contentInset = UIEdgeInsetsMake(0,10,0,10);
       
        cell.txtVOption1.text=@"  ";
        cell.txtVOption2.text=@"  ";
        cell.txtVOption3.text=@"  ";
        cell.txtVOption4.text=@"  ";
        
        NSLog(@"GET QUESTION %@",[GETQuestions valueForKeyPath:@"options.OptionDescription"][indexPath.row]);
        NSLog(@"GET QUESTION %@",[GETQuestions valueForKeyPath:@"options.OptionDescription"][indexPath.row]);
        cell.txtVOption1.text = [NSString stringWithFormat:@" %@",[GETOptions valueForKey:@"OptionDescription"][indexPath.row][0]];
        
        if ([cell.txtVOption1.text isEqualToString:@"  "]) {
            cell.txtVOption1.hidden = YES;
            cell.lblA.hidden = YES;
     //       cell.btnOption1.userInteractionEnabled = NO;
            
        }
        else{
            cell.txtVOption1.hidden = NO;
            cell.lblA.hidden = NO;
         //   cell.btnOption1.userInteractionEnabled = YES;
        }
        
        cell.txtVOption2.text = [NSString stringWithFormat:@" %@",[GETOptions valueForKey:@"OptionDescription"][indexPath.row][1]];
        if ([cell.txtVOption2.text isEqualToString:@"  "]) {
            cell.txtVOption2.hidden = YES;
            cell.lblB.hidden = YES;
          //  cell.bntOption2.userInteractionEnabled = NO;
            
        }
        else{
            cell.txtVOption2.hidden = NO;
            cell.lblB.hidden = NO;
           // cell.bntOption2.userInteractionEnabled = YES;
        }
        
        cell.txtVOption3.text = [NSString stringWithFormat:@" %@",[GETOptions valueForKey:@"OptionDescription"][indexPath.row][2]];
        if ([cell.txtVOption3.text isEqualToString:@"  "]) {
            cell.txtVOption3.hidden = YES;
            cell.lblC.hidden = YES;
          //  cell.btnOption3.userInteractionEnabled = NO;
            
        }
        else{
            cell.txtVOption3.hidden = NO;
            cell.lblC.hidden = NO;
          //  cell.btnOption3.userInteractionEnabled = YES;
        }
        
        cell.txtVOption4.text = [NSString stringWithFormat:@" %@",[GETOptions valueForKey:@"OptionDescription"][indexPath.row][3]];
        if ([cell.txtVOption4.text isEqualToString:@"  "]) {
            cell.txtVOption4.hidden = YES;
            cell.lblD.hidden = YES;
         //   cell.btnOption4.userInteractionEnabled = NO;
        }
        else{
            cell.txtVOption4.hidden = NO;
            cell.lblD.hidden = NO;
           // cell.btnOption4.userInteractionEnabled = YES;
        }
        
        
       //  cell.txtVOption1.text=@" Using lorem ipsum to focus attention on graphic elements in a webpage design proposal In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document without relying on meaningful content (also called greeking). Replacing the actual content with placeholder text allows designers to design the form of the content before the content itself has been produced";
        
        
        
        
        op1=[self getLabelHeight:cell.txtVOption1];
        op2=[self getLabelHeight:cell.txtVOption2];
        op3=[self getLabelHeight:cell.txtVOption3];
        op4=[self getLabelHeight:cell.txtVOption4];
        
        NSString * str1 = [NSString stringWithFormat:@"%f",op1];
        NSString * str2 = [NSString stringWithFormat:@"%f",op2];
        NSString * str3 = [NSString stringWithFormat:@"%f",op3];
        NSString * str4 = [NSString stringWithFormat:@"%f",op4];
        NSLog(@"str1 = %@",str1);
        NSLog(@"str2 = %@",str2);
        NSLog(@"str3 = %@",str3);
        NSLog(@"str4 = %@",str4);
        //cell.txtVOption1.textContainerInset = UIEdgeInsetsMake(0, 20, 0, 20);
       
        //**********************************************************
        
        
//        if(cell.viewOption1.frame.origin.y != 500)
//        {
//            CGRect frame1 = cell.viewOption1.frame;
//            //frame1.origin.y = 202;
//            CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
//            frame1.origin.y = cell.txtVQuestion.frame.size.height+30*2+10;
//            cell.viewOption1.frame = frame1;
//            NSString * op123kkkk = [NSString stringWithFormat:@"%f",op123];
//            NSLog(@"op123kkkk = %@",op123kkkk);
//
//        }
//        if(cell.viewOption2.frame.origin.y != 500)
//        {
//            CGRect frame2 = cell.viewOption2.frame;
//            //frame2.origin.y = 260;
//            CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
//            frame2.origin.y = cell.txtVQuestion.frame.size.height + cell.viewOption1.frame.size.height+30*2+10; //+op123+20*2;
//            cell.viewOption2.frame = frame2;
//        }
//
//        if(cell.viewOption3.frame.origin.y != 500)
//        {
//            CGRect frame3 = cell.viewOption3.frame;
//            //frame3.origin.y = 317;
//            CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
//            frame3.origin.y = cell.txtVQuestion.frame.size.height + cell.viewOption1.frame.size.height +cell.viewOption2.frame.size.height+30*2+10;
//            cell.viewOption3.frame = frame3;
//        }
//
//        if(cell.viewOption4.frame.origin.y != 500)
//        {
//            CGRect frame4 = cell.viewOption4.frame;
//            //frame4.origin.y = 376;
//            CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
//            frame4.origin.y = cell.txtVQuestion.frame.size.height + cell.viewOption1.frame.size.height + cell.viewOption2.frame.size.height + cell.viewOption3.frame.size.height+30*2+10;
//            cell.viewOption4.frame = frame4;
//        }
        
        if(cell.viewOption1.frame.origin.y != 500)
        {
            CGRect frame1 = cell.viewOption1.frame;
            //frame1.origin.y = 202;
            CGFloat op123 =[self getLabelHeight:cell.txtVOption1];
            frame1.origin.y = cell.txtVQuestion.frame.origin.y + cell.txtVQuestion.frame.size.height+30*2;
            frame1.size.height = op1;

           
            // [cell.lblA setFrame:CGRectMake(cell.lblA.frame.origin.x, frame1.origin.y, cell.lblA.frame.size.width, cell.lblA.frame.size.height)];
            //                frame1.origin.y = cell.txtVQuestion.frame.size.height +op123+20*2;
            cell.viewOption1.frame = frame1;
            // cell.btnOption1.frame = cell.viewOption1.frame;
            NSString * op123kkkk = [NSString stringWithFormat:@"%f",op123];
            NSLog(@"op123kkkk = %@",op123kkkk);
            [cell.btnOption1 layoutIfNeeded];
        }

        if(cell.viewOption2.frame.origin.y != 500)
        {
            CGRect frame2 = cell.viewOption2.frame;
            //frame2.origin.y = 260;
            CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
            frame2.size.height = op2;
           // cell.bntOption2.frame = cell.viewOption2.frame;
            //[cell.lblB setFrame:CGRectMake(cell.lblA.frame.origin.x, frame2.origin.y, cell.lblA.frame.size.width, cell.lblA.frame.size.height)];
            frame2.origin.y = cell.txtVQuestion.frame.size.height + cell.viewOption1.frame.size.height+30*2+10;
            cell.viewOption2.frame = frame2;
        }

        if(cell.viewOption3.frame.origin.y != 500)
        {
            CGRect frame3 = cell.viewOption3.frame;
            //frame3.origin.y = 317;
            CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
            frame3.size.height = op3;
           // cell.btnOption3.frame = cell.viewOption3.frame;
            //[cell.lblC setFrame:CGRectMake(cell.lblA.frame.origin.x, frame3.origin.y, cell.lblA.frame.size.width, cell.lblA.frame.size.height)];
            frame3.origin.y = cell.txtVQuestion.frame.size.height + cell.viewOption1.frame.size.height +cell.viewOption2.frame.size.height+30*2+20;
            cell.viewOption3.frame = frame3;
        }

        if(cell.viewOption4.frame.origin.y != 500)
        {
            CGRect frame4 = cell.viewOption4.frame;
            //frame4.origin.y = 376;
            CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
            frame4.size.height = op4;
           // cell.btnOption4.frame = cell.viewOption4.frame;
            // [cell.lblD setFrame:CGRectMake(cell.lblA.frame.origin.x, frame4.origin.y, cell.lblA.frame.size.width, cell.lblA.frame.size.height)];
            frame4.origin.y = cell.txtVQuestion.frame.size.height + cell.viewOption1.frame.size.height + cell.viewOption2.frame.size.height + cell.viewOption3.frame.size.height+30*2+30;
            cell.viewOption4.frame = frame4;
        }


        CGRect viewback = cell.Vw_Back.frame;
        viewback.size.height = cell.viewOption4.frame.origin.y + cell.viewOption4.frame.size.height + 20;
        cell.Vw_Back.frame = viewback;

        cell.scrollVQuestion.contentSize =CGSizeMake(cell.contentView.frame.size.width, viewback.size.height+10);
        
       // [collectionView reloadItemsAtIndexPaths:@[indexPath]];
        
        //        cell.scrollVQuestion.contentSize =CGSizeMake(cell.contentView.frame.size.width, 400);
        //        CGRect frame12 = cell.contentView.frame;
        //        frame12.size.height = 400;
        //        cell.contentView.frame = frame12;
        
        //[collectionView reloadItemsAtIndexPaths:@[indexPath]];
        
        //[cell.scrollVQuestion setNeedsDisplay];
        
        //**********************************************************
        

//        if (op1>50) {
//            CGRect newFrame3 = cell.txtVOption1.frame;
//            newFrame3.size.height = op1;
//            cell.txtVOption1.frame = newFrame3;
//
//
//        }
//
//        if (op2>50) {
//            CGRect newFrame3 = cell.txtVOption2.frame;
//            newFrame3.size.height = op2;
//            cell.txtVOption2.frame = newFrame3;
//        }
//
//        if (op3>50) {
//            CGRect newFrame3 = cell.txtVOption3.frame;
//            newFrame3.size.height = op3;
//            cell.txtVOption3.frame = newFrame3;
//        }
//
//        if (op4>50) {
//            CGRect newFrame3 = cell.txtVOption4.frame;
//            newFrame3.size.height = op4;
//            cell.txtVOption4.frame = newFrame3;
//        }
        
        
        
        NSLog(@"GETOptions %@",[GETOptions valueForKey:@"is_selected"]);
        if (![[NSString stringWithFormat:@"%@",[GETOptions valueForKey:@"is_selected"][indexPath.row][0]] isEqualToString:@"0"]){
            
            cell.btn_flag.userInteractionEnabled = YES;
            [self selectedOptionColor:cell.lblA];
            [self unSelectedOptionColor:cell.lblB :cell.lblC :cell.lblD];
            BOOL isTheObjectThere = [marrFlag2 containsObject: [GETQuestions valueForKey:@"TestDTLID"][indexPath.row]];
            if (isTheObjectThere == NO) {
                [marrFlag1 addObject:[GETOptions valueForKey:@"QuestionOptionID"][indexPath.row][0]];
                [marrFlag2 addObject:[GETQuestions valueForKey:@"TestDTLID"][indexPath.row]];
            }
        }
        else if (![[NSString stringWithFormat:@"%@",[GETOptions valueForKey:@"is_selected"][indexPath.row][1]] isEqualToString:@"0"]){
            cell.btn_flag.userInteractionEnabled = YES;
            [self selectedOptionColor:cell.lblB ];
            [self unSelectedOptionColor:cell.lblA :cell.lblC :cell.lblD];
            BOOL isTheObjectThere = [marrFlag2 containsObject: [GETQuestions valueForKey:@"TestDTLID"][indexPath.row]];
            if (isTheObjectThere == NO) {
                [marrFlag1 addObject:[GETOptions valueForKey:@"QuestionOptionID"][indexPath.row][1]];
                [marrFlag2 addObject:[GETQuestions valueForKey:@"TestDTLID"][indexPath.row]];
            }
        }
        else if (![[NSString stringWithFormat:@"%@",[GETOptions valueForKey:@"is_selected"][indexPath.row][2]] isEqualToString:@"0"])
        {
            cell.btn_flag.userInteractionEnabled = YES;
            [self selectedOptionColor:cell.lblC ];
            [self unSelectedOptionColor:cell.lblA :cell.lblB :cell.lblD];
            BOOL isTheObjectThere = [marrFlag2 containsObject: [GETQuestions valueForKey:@"TestDTLID"][indexPath.row]];
            if (isTheObjectThere == NO) {
                
                [marrFlag1 addObject:[GETOptions valueForKey:@"QuestionOptionID"][indexPath.row][2]];
                [marrFlag2 addObject:[GETQuestions valueForKey:@"TestDTLID"][indexPath.row]];
                
            }
        }
        else if (![[NSString stringWithFormat:@"%@",[GETOptions valueForKey:@"is_selected"][indexPath.row][3]] isEqualToString:@"0"])
        {
            cell.btn_flag.userInteractionEnabled = YES;
            [self selectedOptionColor:cell.lblD ];
            [self unSelectedOptionColor:cell.lblA :cell.lblB :cell.lblC];
            BOOL isTheObjectThere = [marrFlag2 containsObject: [GETQuestions valueForKey:@"TestDTLID"][indexPath.row]];
            if (isTheObjectThere == NO) {
                [marrFlag1 addObject:[GETOptions valueForKey:@"QuestionOptionID"][indexPath.row][3]];
                [marrFlag2 addObject:[GETQuestions valueForKey:@"TestDTLID"][indexPath.row]];
            }
        }
        else
        {
            
            [self unSelectedOptionColor1:cell.txtVOption1 :cell.txtVOption2 :cell.txtVOption3 :cell.txtVOption4];
            
            [self unSelectedOptionColor1:cell.lblA :cell.lblB :cell.lblC :cell.lblD];
        
            
            cell.btnOption1.tag = indexPath.row;
            cell.btnOption1.userInteractionEnabled = YES;
            cell.bntOption2.tag = indexPath.row;
            cell.bntOption2.userInteractionEnabled = YES;
            cell.btnOption3.tag = indexPath.row;
            cell.btnOption3.userInteractionEnabled = YES;
            cell.btnOption4.tag = indexPath.row;
            cell.btnOption4.userInteractionEnabled = YES;
         
            [cell.btnOption1 addTarget:self action:@selector(selectedAnswer1:) forControlEvents:UIControlEventTouchUpInside];
            [cell.bntOption2 addTarget:self action:@selector(selectedAnswer2:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnOption3 addTarget:self action:@selector(selectedAnswer3:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnOption4 addTarget:self action:@selector(selectedAnswer4:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        
        NSLog(@"marrFlag %@",marrFlag);
        
        if (cell.btn_flag.userInteractionEnabled == YES) {
            if ([[marrFlag objectAtIndex:indexPath.row] intValue]==2) {
                cell.btn_flag.userInteractionEnabled = NO;
                cell.btn_flag.selected=NO;
                [cell.btn_flag setImage:[UIImage imageNamed:@"mark-blue"] forState:UIControlStateNormal];
            }
            else{
                //FLAG
                cell.btn_flag.tag=indexPath.row;
                cell.btn_flag.userInteractionEnabled = YES;
                [cell.btn_flag addTarget:self action:@selector(selectedAnswer5:) forControlEvents:UIControlEventTouchUpInside];
                
                
                if ([[marrFlag objectAtIndex:indexPath.row] intValue]==0) {
                    
                    cell.btn_flag.selected=NO;
                    [cell.btn_flag setImage:[UIImage imageNamed:@"mark-grey"] forState:UIControlStateNormal];
                }
                else
                {
                    BOOL isTheObjectThere = [marrFlag1 containsObject: [GETQuestions valueForKey:@"QuestionID"][indexPath.row]];
                    if (isTheObjectThere == NO) {
                        [marrFlag1 addObject:@"0"];
                        [marrFlag2 addObject:[GETQuestions valueForKey:@"TestDTLID"][indexPath.row]];
                    }
                    cell.btn_flag.selected=YES;
                }
                
            }
        }
        else{
            if ([[marrFlag objectAtIndex:indexPath.row] intValue]==2) {
                cell.btn_flag.userInteractionEnabled = NO;
                cell.btn_flag.selected=NO;
                [cell.btn_flag setImage:[UIImage imageNamed:@"mark-blue"] forState:UIControlStateNormal];
            }
        }
        
        Lbl_Score.hidden = YES;
        return cell;
        
        
    }
    
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height+20;
}

- (void) updateLabelPosition: (UIView*) optionView : (int) pos {
    if(optionView.frame.origin.y != pos)
    {
        CGRect frame1 = cell.viewOption1.frame;
        //frame1.origin.y = pos;
        //optionView.frame = frame1;
        
        CGFloat op123 =[self getLabelHeight:cell.txtVQuestion];
        frame1.origin.y = cell.txtVQuestion.frame.size.height + op123 - 30*2+20;
        optionView.frame = frame1;
        NSString * op123kkkk = [NSString stringWithFormat:@"%f",op123];
        NSLog(@"op123kkkk = %@",op123kkkk);
        
    }
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(collectionView == self.collDispQuestionNo){
        
        [self moveToQuestion:indexPath];
        
    }else{
        
        [GETQuestions objectAtIndex:7];
        
        
    }
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(collectionView == self.collDispQuestionNo){
        
        return CGSizeMake((self.collDispQuestionNo.frame.size.width/10), (self.collDispQuestionNo.frame.size.height));
        
    }
    else{
        
        return CGSizeMake((self.collDispQuestions.frame.size.width),(self.collDispQuestions.frame.size.height));
        
    }
    
}

#pragma mark - UIScrollView Method


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (scrollView == _collDispQuestionNo) {
        
        NSLog(@"_collDispQuestionNo ");
        
    }else
    {
  
        NSLog(@"scrollViewDidEndDecelerating");
        
        if (self.lastContentOffset > scrollView.contentOffset.x){
            
            [self moveToPreviousQuestion];
            
        }
        else if(self.lastContentOffset < scrollView.contentOffset.x) {
            
            [self moveToNextQuestion];
            
        }
    }
   
    _lastContentOffset = scrollView.contentOffset.x;
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
    NSLog(@"scrollViewDidEndScrollingAnimation");
    
    _lastContentOffset = scrollView.contentOffset.x;
    NSLog(@"lastContentOffset : %f",_lastContentOffset);
    
    
}

#pragma mark - Change Other Method
/*
 -(void)changeSelection:(int )index{
 
 
 if (![[[ShareData.marrmcqquestion objectAtIndex:index] valueForKey:@"selectedAns"] isEqual:@"0"]) {
 
 int  SelectedAnswer = [[[ShareData.marrmcqquestion objectAtIndex:index] valueForKey:@"selectedAns"] intValue];
 NSLog(@"selected Answer : %d",SelectedAnswer);
 
 switch (SelectedAnswer) {
 case 1:
 
 [self selectedOptionColor:cell.lblA ];
 [self unSelectedOptionColor:cell.lblB :cell.lblC  :cell.lblD];
 
 break;
 
 case 2:
 
 [self selectedOptionColor:cell.lblB];
 [self unSelectedOptionColor:cell.lblA :cell.lblC  :cell.lblD ];
 break;
 
 case 3:
 
 [self selectedOptionColor:cell.lblC ];
 [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblD ];
 break;
 
 case 4:
 
 
 [self selectedOptionColor:cell.lblD ];
 [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblC ];
 
 break;
 
 default:
 break;
 
 }
 
 }
 else{
 
 cell.lblA.layer.backgroundColor = [UIColor clearColor].CGColor;
 cell.lblB.layer.backgroundColor = [UIColor clearColor].CGColor;
 cell.lblC.layer.backgroundColor = [UIColor clearColor].CGColor;
 cell.lblD.layer.backgroundColor = [UIColor clearColor].CGColor;
 
 
 }
 
 }
 
 */

#pragma mark - Convert HTML Formated String
-(NSAttributedString *)convertIntoAttributadStr:(NSString *)str{
    
    NSString *htmlString = str;
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    
    
    return attributedString;
}



#pragma mark - Change Height Of Textview

- (void)changeTextViewHeight:(UITextView *)textView :(NSLayoutConstraint*)heightConstraint
{
    NSLog(@"changeTextViewHeight");
    
    
    CGSize constraint = CGSizeMake(textView.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [textView.text boundingRectWithSize:constraint
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:textView.font}
                                                     context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    heightConstraint.constant = size.height + 20;
    
    NSLog(@"height : %f",size.height);
    
    
    
}

#pragma mark - change Webview height

- (void)changeheightOfWebview:(UIWebView *)aWebView :(NSLayoutConstraint*)heightConstraint{
    
    NSString *str = [aWebView stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
    CGFloat height = str.floatValue;
    heightConstraint.constant = height + 15.0;
    NSLog(@"height: %f", height);
}


#pragma mark - Webview Delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    
    if (webView.tag == 1) {
        
        if ([strQuestion isEqualToString:@"Web"]) {
            [self changeheightOfWebview:cell.webVQuestion :cell.viewQuestionHeight];
        }
    }
    
    if (webView.tag == 2) {
        
        if ([strOption1 isEqualToString:@"Web"]){
            [self changeheightOfWebview:cell.webVOption1 :cell.viewOption1Height];
        }
    }
    
    if (webView.tag == 3) {
        
        if ([strOption2 isEqualToString:@"Web"]){
            [self changeheightOfWebview:cell.webVOption2 :cell.viewOption2Height];
        }
    }
    
    if (webView.tag == 4) {
        
        if ([strOption3 isEqualToString:@"Web"]){
            [self changeheightOfWebview:cell.webVOption3 :cell.viewOption3Height];
        }
    }
    
    if (webView.tag == 5) {
        
        if ([strOption4 isEqualToString:@"Web"]){
            [self changeheightOfWebview:cell.webVOption4 :cell.viewOption4Height];
        }
    }
}

#pragma mark - Check Question and Option String

- (NSString*)displayWebviewORTextView:(NSString *)Str Webview:(UIWebView*)Webview TextView:(UITextView*)TextView BGView:(NSLayoutConstraint*)BGViewHeightConstant {
    
    if ([Str containsString:@"<img"]|| [Str containsString:@"<math"] || [Str containsString:@"<table"] ) {
        
        NSLog(@"string contains tag!");
        if ([Str containsString:@"src"]) {
            Str = [Str stringByReplacingOccurrencesOfString:@"src=\"/" withString:@"src=\"http://staff.parshvaa.com/"];
            NSLog(@"apdateString %@",Str);
        }
        TextView.hidden =true;
        Webview.hidden =false;
        [Webview loadHTMLString:Str baseURL:nil];
        
        return @"Web";
        
    } else {
        
        NSLog(@"string does not contain tags");
        Webview.hidden =true;
        TextView.hidden =false;
        TextView.attributedText = [self convertIntoAttributadStr:[NSString stringWithFormat:@"%@",Str]];
        [self changeTextViewHeight:TextView :BGViewHeightConstant];
        return @"Text";
    }
}




#pragma mark - Action Method

- (IBAction)btnBack:(id)sender {
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Are you sure you want to Leave the test?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * yes = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    UIAlertAction * no = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:no];
    [alert addAction:yes];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)bntPrevious:(id)sender {
    
    NSLog(@"btnPrevious tap");
    
    [self moveToPreviousQuestion];
    
}

- (IBAction)btnNext:(id)sender {
    
    NSLog(@"btnNext tap");
    [self moveToNextQuestion];
    
}

- (IBAction)SaveandExitButtonPressed:(id)sender {
    [self SubmitTestDetails:@"0"];
    //GetView.hidden = YES;
    gotoFlag=0;
    
}

- (IBAction)ExitButtonPressed:(id)sender {
    [self SubmitTestDetails:@"1"];
    gotoFlag=1;
    //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)ExitQuizButtonPressed:(id)sender {
    GetView.hidden = NO;
}

#pragma mark - Move NEXT_PREVIOUS Method

-(void)moveToNextQuestion{
    
    NSLog(@"moveToNextQuestion");
    curruntRow++;
    
    if(curruntRow == GETQuestions.count - 1){
        
        _btnNext.enabled = false;
        _btnPrevious.enabled = true;
        
    }else{
        
        if(curruntRow > 0 ){
            
            _btnPrevious.enabled = true;
        }
    }
    
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:curruntRow inSection:0];
    
    [self.collDispQuestions scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self.collDispQuestionNo scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    [self changeQuestionNo:curruntRow+1];
    
    NSLog(@"curruntRow %d",curruntRow);
    
    [self.collDispQuestionNo reloadData];
    
}

-(void)moveToPreviousQuestion{
    
    NSLog(@"moveToPreviousQuestion");
    curruntRow--;
    
    if(curruntRow == 0){
        _btnNext.enabled=true;
        _btnPrevious.enabled = false;
        
    }else{
        
        if(curruntRow < GETQuestions.count){
            
            _btnNext.enabled=true;
            
        }
    }
    
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:curruntRow inSection:0];
    
    [self.collDispQuestions scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self.collDispQuestionNo scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    [self changeQuestionNo:curruntRow+1];
    NSLog(@"curruntRow %d",curruntRow);
    
    [self.collDispQuestionNo reloadData];
      [_collDispQuestions reloadData];
    
}

-(void)moveToQuestion:(NSIndexPath*)indexpath{
    
    NSLog(@"Indexpath : %ld",(long)indexpath.row);
    
    curruntRow = (int)indexpath.row;
    
    if (curruntRow > 0) {
        
        _btnPrevious.enabled = true;
        
        if(curruntRow == GETQuestions.count - 1){
            
            _btnNext.enabled = false;
            _btnPrevious.enabled = true;
            
        }else{
            
            if(curruntRow > 0 ){
                
                _btnPrevious.enabled = true;
            }
        }
        
    }else{
        
        _btnPrevious.enabled = false;
    }
    
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:curruntRow inSection:0];
    
    [self.collDispQuestions scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    [self.collDispQuestionNo scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
    
    [self changeQuestionNo:curruntRow+1];
    
    [self.collDispQuestionNo reloadData];
      [_collDispQuestions reloadData];
    //[self.collDispQuestions reloadData];
}

- (void)changeQuestionNo:(int)questionNo{
    
    
    int j=0;
    for (int i=0; i<arrAttamt.count; i++) {
        if ([[arrAttamt objectAtIndex:i] intValue]==1) {
            j++;
        }
    }
    // _lbl_attemp.text=[NSString stringWithFormat:@"%02d",j];
    
    NSMutableAttributedString *CreateAcc;
    NSRange linkRange2;
    CreateAcc = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attempt %02d / %lu",j,arrAttamt.count] attributes:nil];
    NSString *AccString = [NSString stringWithFormat:@"Attempt %02d / %lu",j,arrAttamt.count];
    linkRange2 = [AccString rangeOfString:[NSString stringWithFormat:@"%02d", j]];
    
    
    NSDictionary *linkAttributes2 = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:0.05 green:0.4 blue:0.65 alpha:1.0]};
    [CreateAcc setAttributes:linkAttributes2 range:linkRange2];
    
    // Assign attributedText to UILabel
    _lblDispQuestionNo.attributedText = CreateAcc;
}

#pragma mark - ANSWER Select_Unselect Method

-(void)selectedOptionColor:(UILabel*)label{
    label.textColor = [UIColor whiteColor];
    label.layer.backgroundColor = [UIColor colorWithRed:3.0/255.0 green:152.0/255.0 blue:230.0/255.0 alpha:1.0].CGColor;
    label.layer.cornerRadius = label.frame.size.height/2;
    
    //[self.collDispQuestions reloadData];
}

-(void)selectedRightOptionColor:(UILabel*)label{
    label.textColor = [UIColor whiteColor];
    label.layer.backgroundColor = [UIColor greenColor].CGColor;
    label.layer.cornerRadius = label.frame.size.height/2;
}

-(void)selectedRightOptionColor1:(UILabel*)label : (NSInteger)getnew : (NSInteger)getIndex
{
    //GETOptions = [[NSMutableArray alloc] init];
    //GETOptions = [GETQuestions valueForKey:@"options"];
    
    /////Attamt count
    
    if ([[arrAttamt objectAtIndex:curruntRow] intValue]==0) {
        [arrAttamt replaceObjectAtIndex:curruntRow withObject:@"1"];
        
    }
    
    NSLog(@"arrAttamt %lu",arrAttamt.count);
    
    int j=00;
    for (int i=0; i<arrAttamt.count; i++) {
        if ([[arrAttamt objectAtIndex:i] intValue]==1) {
            j++;
        }
    }
   // _lbl_attemp.text=[NSString stringWithFormat:@"%02d",j];
    
    NSMutableAttributedString *CreateAcc;
    NSRange linkRange2;
    CreateAcc = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attempt %02d / %lu",j,arrAttamt.count] attributes:nil];
    NSString *AccString = [NSString stringWithFormat:@"Attempt %02d / %lu",j,arrAttamt.count];
    linkRange2 = [AccString rangeOfString:[NSString stringWithFormat:@"%02d", j]];
    
    
    NSDictionary *linkAttributes2 = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:0.05 green:0.4 blue:0.65 alpha:1.0]};
    [CreateAcc setAttributes:linkAttributes2 range:linkRange2];
    
    // Assign attributedText to UILabel
    _lblDispQuestionNo.attributedText = CreateAcc;
    //////
    
    
    
    
     myturn = NO;
    for (int i = 0; i<=3; i++)
    {
        if ([[GETOptions[curruntRow][i] valueForKey:@"is_selected"] isEqualToString:@"1"]) {
            NSLog(@"hello");
            
            myturn = YES;
            NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
            NSDictionary *oldDict = (NSDictionary *)[GETQuestions valueForKeyPath:@"options"][curruntRow][i];
            [newDict addEntriesFromDictionary:oldDict];
            [newDict setObject:@"0" forKey:@"is_selected"];
            
            NSMutableArray *getarr = [[NSMutableArray alloc] init];
            for (NSString *str in GETOptions[curruntRow]) {
                [getarr addObject:str];
            }
            
            [getarr replaceObjectAtIndex:i withObject:newDict];
            
            NSMutableArray *get = [[NSMutableArray alloc] init];
            for (NSString *str in GETOptions) {
                [get addObject:str];
            }
            [get replaceObjectAtIndex:curruntRow withObject:getarr];
            
            GETOptions = [[NSMutableArray alloc] init];
            for (NSString *str in get) {
                [GETOptions addObject:str];
            }
            
            NSMutableDictionary *newDict1 = [[NSMutableDictionary alloc] init];
            NSDictionary *oldDict1 = (NSDictionary *)[GETQuestions valueForKeyPath:@"options"][curruntRow][getIndex];
            [newDict1 addEntriesFromDictionary:oldDict1];
            [newDict1 setObject:@"1" forKey:@"is_selected"];
            
            NSMutableArray *getarr1 = [[NSMutableArray alloc] init];
            for (NSString *str in GETOptions[curruntRow]) {
                [getarr1 addObject:str];
            }
            
            [getarr1 replaceObjectAtIndex:getIndex withObject:newDict1];
            
            NSMutableArray *get1 = [[NSMutableArray alloc] init];
            for (NSString *str in GETOptions) {
                [get1 addObject:str];
            }
            [get1 replaceObjectAtIndex:curruntRow withObject:getarr1];
            
            GETOptions = [[NSMutableArray alloc] init];
            for (NSString *str in get1) {
                [GETOptions addObject:str];
            }
        }
    }
    
    if (myturn == NO) {
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[GETQuestions valueForKeyPath:@"options"][getnew][getIndex];
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:@"1" forKey:@"is_selected"];
        
        NSMutableArray *getarr = [[NSMutableArray alloc] init];
        for (NSString *str in GETOptions[getnew]) {
            [getarr addObject:str];
        }
        [getarr replaceObjectAtIndex:getIndex withObject:newDict];
        
        NSMutableArray *get = [[NSMutableArray alloc] init];
        for (NSString *str in GETOptions) {
            [get addObject:str];
        }
        
        [get replaceObjectAtIndex:getnew withObject:getarr];
        
        GETOptions = [[NSMutableArray alloc] init];
        for (NSString *str in get) {
            [GETOptions addObject:str];
        }
    }
    
    [_collDispQuestions reloadData];
    
    
}

-(void)selectedWrongOptionColor:(UILabel*)label{
    //label.textColor = [UIColor whiteColor];
    label.layer.backgroundColor = [UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1.0].CGColor;
    //label.layer.cornerRadius = label.frame.size.height/2;
}

-(void)unSelectedOptionColor:(UILabel*)label1 :(UILabel*)label2 :(UILabel*)label3{
    label1.textColor = [UIColor darkGrayColor];
    label2.textColor = [UIColor darkGrayColor];
    label3.textColor = [UIColor darkGrayColor];
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
    label3.layer.backgroundColor = [UIColor clearColor].CGColor;
}

-(void)unSelectedOptionColor2:(UILabel*)label1 :(UILabel*)label2{
    label1.textColor = [UIColor darkGrayColor];
    label2.textColor = [UIColor darkGrayColor];
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
}

-(void)unSelectedOptionColor1:(UILabel*)label1 :(UILabel*)label2 :(UILabel*)label3 :(UILabel*)label4{
   
    label1.textColor = [UIColor darkGrayColor];
    label2.textColor = [UIColor darkGrayColor];
    label3.textColor = [UIColor darkGrayColor];
    label4.textColor = [UIColor darkGrayColor];
    
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
    label3.layer.backgroundColor = [UIColor clearColor].CGColor;
    label4.layer.backgroundColor = [UIColor clearColor].CGColor;
    
}

-(void)selectedAnswer1:(UIButton *)sender{
    [self selectedOptionColor:cell.lblA ];
    [self selectedRightOptionColor1:cell.lblA :[sender tag] :0];
   //[self unSelectedOptionColor:cell.txtVOption2 :cell.txtVOption3  :cell.txtVOption4 ];
    [self unSelectedOptionColor:cell.lblB :cell.lblC  :cell.lblD ];
}
-(void)selectedAnswer2:(UIButton *)sender{
    [self selectedOptionColor:cell.lblB];
    [self selectedRightOptionColor1:cell.lblB :[sender tag] :1];
    //[self unSelectedOptionColor:cell.txtVOption1 :cell.txtVOption3  :cell.txtVOption4 ];
    [self unSelectedOptionColor:cell.lblA :cell.lblC  :cell.lblD ];
}

-(void)selectedAnswer3:(UIButton *)sender{
    [self selectedOptionColor:cell.lblC];
    [self selectedRightOptionColor1:cell.lblC :[sender tag] :2];
    //[self unSelectedOptionColor:cell.txtVOption1 :cell.txtVOption2  :cell.txtVOption4 ];
    [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblD ];
}

-(void)selectedAnswer4:(UIButton *)sender{
    [self selectedOptionColor:cell.lblD ];
    [self selectedRightOptionColor1:cell.lblD :[sender tag] :3];
    //[self unSelectedOptionColor:cell.txtVOption1 :cell.txtVOption2  :cell.txtVOption3];
    [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblC ];
}
-(void)selectedAnswer5:(UIButton *)sender{
    
    UIButton *button = (UIButton *)sender;
    
    if ([[marrFlag objectAtIndex:sender.tag] intValue]==0) {
        [marrFlag replaceObjectAtIndex:sender.tag withObject:@"1"];
    }
    else
    {
        [marrFlag replaceObjectAtIndex:sender.tag withObject:@"0"];
    }
    
    
    NSLog(@"---%@",marrFlag);
    if (button.selected == YES) {
        button.selected = NO;
    }
    else{
        button.selected = YES;
    }
    
    [_collDispQuestionNo reloadData];
    [_collDispQuestions reloadData];
    
    
    
}






- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark - SWRevealViewController Delegate

//revealController didMoveToPosition
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft)
    {
        revealController.frontViewController.view.alpha = 1;
        [self.revealViewController tapGestureRecognizer];
        self.view.userInteractionEnabled = YES;
    }
    else
    {
        revealController.frontViewController.view.alpha = 0.5;
        
        self.view.userInteractionEnabled = NO;
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
}
@end
