//
//  QuestionView.h
//  McqApp
//
//  Created by My Mac on 3/9/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "HttpWrapper.h"

@interface QuestionView : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,UIWebViewDelegate,SWRevealViewControllerDelegate,HttpWrapperDelegate>
{
    //WrapperClass Object
    HttpWrapper *httpQuestion;
    
    NSMutableArray *GETQuestions;
    NSMutableArray *GETOptions;
    
    NSMutableArray *marrFlag;
    NSMutableArray *marrFlag1;
    NSMutableArray *arrAttamt;
    
    //View
    IBOutlet UIView *GetView;
    IBOutlet UIView *ExitView;
    IBOutlet UIView *InfoView;
    IBOutlet UIView *DismissView;
    
    
    //Label
    IBOutlet UILabel *Lbl_Score;
    
}

@property (retain, nonatomic) NSString *GetChapterName;
@property (retain, nonatomic) NSString *GetQuestionNumber;
@property (assign) BOOL GETSTUDY;
@property(nonatomic,assign)NSString * GetChapterID;
@property(nonatomic,assign)NSString * subjectId;
@property(nonatomic,assign)NSString * subjectName;
@property(nonatomic,assign)NSString * chepterId;
@property(nonatomic,assign)int levelId;



@property (weak, nonatomic) IBOutlet UICollectionView *collDispQuestions;
@property (weak, nonatomic) IBOutlet UICollectionView *collDispQuestionNo;


@property (weak, nonatomic) IBOutlet UILabel *lblUserName;



@property (weak, nonatomic) IBOutlet UIButton *btnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblDispQuestionNo;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;


- (IBAction)btnBack:(id)sender;
- (IBAction)bntPrevious:(id)sender;
- (IBAction)btnNext:(id)sender;
- (IBAction)btnSubmit:(id)sender;

- (IBAction)SaveandExitButtonPressed:(id)sender;
- (IBAction)ExitButtonPressed:(id)sender;
- (IBAction)ExitQuizButtonPressed:(id)sender;

@end
