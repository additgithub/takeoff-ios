////
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "StudySelectionViewController.h"

@interface StudySelectionViewController ()

@end

@implementation StudySelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    //SideBar Added
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController setDelegate:self];
    if ( revealViewController )
    {
        [Btn_Side addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    //Using RememberMe Checkbox
    group = [BEMCheckBoxGroup groupWithCheckBoxes:@[CheckFirst, CheckSecond]];
    group.selectedCheckBox = CheckFirst; // Optionally set which checkbox is pre-selected
    GetCheck = @"CheckFirst";
    group.mustHaveSelection = YES; // Define if the group must always have a selection
    
    StudyList = [[NSArray alloc] initWithObjects:@"Aerodinamica PP Avion",@"Fisiologica PP Avion",@"Instrumentos de vuelo PP Avion",@"Meteorologia PP Avion",@"Motores PP Avion", nil];
    Btn_SelectChapters.layer.cornerRadius = 5.0;
    Btn_Study.layer.cornerRadius = 5.0;
    
    [self CallMyMethod];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)CallMyMethod
{
    [[AppDelegate sharedAppDelegate] showProgress];
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpStudy  = [[HttpWrapper alloc] init];
        httpStudy.delegate=self;
        httpStudy.getbool=YES;
        [httpStudy requestWithMethod:@"GET" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,GETChapterAPI] param:nil];
    });
    
}

#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    NSLog(@"dict:%@",dicsResponse);
    
    [[AppDelegate sharedAppDelegate] hideProgress];
    if ([[dicsResponse valueForKey:@"status"] boolValue] == true) {
        StudyList = [dicsResponse valueForKey:@"data"];
        
        if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
        {
            NSLog(@"iphone4");
            [self showPopUpWithTitle:@"Select Chapters" withOption:[StudyList valueForKey:@"ChapterName"] xy:CGPointMake(Btn_SelectChapters.frame.origin.x, Btn_SelectChapters.frame.origin.y+35) size:CGSizeMake(Btn_SelectChapters.frame.size.width, 100) isMultiple:NO];
        }
        else{
            [self showPopUpWithTitle:@"Select Chapters" withOption:[StudyList valueForKey:@"ChapterName"] xy:CGPointMake(Btn_SelectChapters.frame.origin.x, Btn_SelectChapters.frame.origin.y+35) size:CGSizeMake(Btn_SelectChapters.frame.size.width, 190) isMultiple:NO];
        }
    }
    else
    {
        [[AppDelegate sharedAppDelegate] hideProgress];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
    }
}

//fetchDataFail
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
    [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:FAIL_DATA_MSG];
    NSLog(@"Fetch Data Fail Error:%@",error);
}

#pragma mark - SWRevealViewController Delegate

//revealController didMoveToPosition
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft)
    {
        revealController.frontViewController.view.alpha = 1;
        [self.revealViewController tapGestureRecognizer];
        self.view.userInteractionEnabled = YES;
    }
    else
    {
        revealController.frontViewController.view.alpha = 0.5;
        
        self.view.userInteractionEnabled = NO;
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
}

#pragma mark - DropDown

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDown_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/

    [Btn_SelectChapters setTitle:[[StudyList valueForKey:@"ChapterName"] objectAtIndex:anIndex] forState:UIControlStateNormal];
    [Btn_SelectChapters setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    GetChapID = [[StudyList valueForKey:@"ChapterID"] objectAtIndex:anIndex];
}

- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
//    if (ArryData.count>0) {
//        NSLog(@"list %@",[ArryData componentsJoinedByString:@"--"]);
//
//        NSLog(@"list %@",[ArryData componentsJoinedByString:@"\n"]);
//
//        StudySelect = [ArryData componentsJoinedByString:@","];
//        [Btn_SelectChapters setTitle:[ArryData componentsJoinedByString:@", "] forState:UIControlStateNormal];
//        [Btn_SelectChapters setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    }
//    else{
//        [Btn_SelectChapters setTitle:@"Select Chapters" forState:UIControlStateNormal];
//    }
    
}
- (void)DropDownListViewDidCancel{
    
}

#pragma mark - Button Action
- (IBAction)StartButtonPressed:(id)sender {
    if (![Btn_SelectChapters.titleLabel.text isEqualToString:@"Select Chapters"])
    {
        QuestionView * next = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionView"];
        next.subjectId = @"1";
        next.chepterId = @"1,2,4,5,6";
        next.levelId = 1;
        next.subjectName = @"english";
        next.GETSTUDY = YES;
        next.GetChapterID = GetChapID;
        if (CheckFirst.on) {
            next.GetQuestionNumber = @"0";
        }
        else{
            next.GetQuestionNumber = @"1";
        }
        next.GetChapterName = Btn_SelectChapters.titleLabel.text;
        [self.navigationController pushViewController:next animated:YES];
    }
    else{
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:@"Please select any chapter"];
    }
}

- (IBAction)ChapterButtonPressed:(id)sender
{
    [Dropobj fadeOut];
    if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
    {
        NSLog(@"iphone4");
        [self showPopUpWithTitle:@"Select Chapters" withOption:[StudyList valueForKey:@"ChapterName"] xy:CGPointMake(Btn_SelectChapters.frame.origin.x, Btn_SelectChapters.frame.origin.y+35) size:CGSizeMake(Btn_SelectChapters.frame.size.width, 100) isMultiple:NO];
    }
    else{
        [self showPopUpWithTitle:@"Select Chapters" withOption:[StudyList valueForKey:@"ChapterName"] xy:CGPointMake(Btn_SelectChapters.frame.origin.x, Btn_SelectChapters.frame.origin.y+35) size:CGSizeMake(Btn_SelectChapters.frame.size.width, 190) isMultiple:NO];
    }
    
}
@end
