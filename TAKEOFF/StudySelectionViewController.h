////
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMCheckBox.h"
#import "SWRevealViewController.h"
#import "SidebarViewController.h"
#import "DropDownListView.h"
#import "QuestionView.h"
#import "HttpWrapper.h"

@interface StudySelectionViewController : UIViewController<BEMCheckBoxDelegate,kDropDownListViewDelegate,SWRevealViewControllerDelegate,HttpWrapperDelegate>
{
    //WrapperClass Object
    HttpWrapper *httpStudy;
    NSString *GetChapID;
    
    NSArray *StudyList;
    NSString *StudySelect;
    NSString *GetCheck;
    BEMCheckBoxGroup*group;
    
    //DropDown
    NSMutableArray *arryList;
    DropDownListView * Dropobj;
    
    //Outlets
    
    //CheckMark
    IBOutlet BEMCheckBox *CheckFirst;
    IBOutlet BEMCheckBox *CheckSecond;
    
    //View
    IBOutlet UIView *Vw_Select;
    
    //Button
    IBOutlet UIButton *Btn_Study;
    IBOutlet UIButton *Btn_Start;
    IBOutlet UIButton *Btn_SelectChapters;
    IBOutlet UIButton *Btn_Side;
}

//Property
@property (strong, nonatomic) SWRevealViewController *viewController;

//Actions
- (IBAction)StartButtonPressed:(id)sender;
- (IBAction)ChapterButtonPressed:(id)sender;


@end
