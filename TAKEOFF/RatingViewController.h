////
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "AppDelegate.h"

@interface RatingViewController : UIViewController<HttpWrapperDelegate>
{
    //WrapperClass Object
    HttpWrapper *httpRating;
    
    
    //Label
    IBOutlet UILabel *Lbl_Date;
    IBOutlet UILabel *Lbl_RightAns;
    IBOutlet UILabel *Lbl_IncorrectAns;
    IBOutlet UILabel *Lbl_MarkedQue;
    IBOutlet UILabel *Lbl_GradePoints;
    IBOutlet UILabel *Lbl_Percentage;
    __weak IBOutlet UILabel *lbl_tot_questions;
    
    
}

@property (retain, nonatomic) NSString *gethaderid;
- (IBAction)btn_BACK:(id)sender;

@end
