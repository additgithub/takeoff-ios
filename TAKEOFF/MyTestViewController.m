////
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "MyTestViewController.h"

@interface MyTestViewController ()

@end

@implementation MyTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    //SideBar Added
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController setDelegate:self];
    if ( revealViewController )
    {
        [Btn_Side addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
   
}

- (void)viewWillAppear:(BOOL)animated
{
     [self CallMyMethod];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)CallMyMethod
{
    [[AppDelegate sharedAppDelegate] showProgress];
    NSString *UserID = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"UserID"];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:UserID forKey:@"user_id"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpTest  = [[HttpWrapper alloc] init];
        httpTest.delegate=self;
        httpTest.getbool=NO;
        [httpTest requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,MYTESTLIST] param:[AddPost copy]];
    });
    
}


#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    NSLog(@"dict:%@",dicsResponse);
    
    [[AppDelegate sharedAppDelegate] hideProgress];
    if ([[dicsResponse valueForKey:@"status"] boolValue] == true) {
        ArrTests = [[NSMutableArray alloc] init];
        
        ArrTests = [dicsResponse valueForKey:@"data"];
        
        if (![ArrTests isEqual:[NSNull null]]) {
            if (ArrTests.count != 0) {
                [Tbl_Main reloadData];
            }
        }
        
        
    }
    else
    {
        [[AppDelegate sharedAppDelegate] hideProgress];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
    }
}

//fetchDataFail
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
    [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:FAIL_DATA_MSG];
    NSLog(@"Fetch Data Fail Error:%@",error);
}


#pragma mark - SWRevealViewController Delegate

//revealController didMoveToPosition
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft)
    {
        revealController.frontViewController.view.alpha = 1;
        [self.revealViewController tapGestureRecognizer];
        self.view.userInteractionEnabled = YES;
    }
    else
    {
        revealController.frontViewController.view.alpha = 0.5;
        
        self.view.userInteractionEnabled = NO;
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
}



#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ArrTests.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    UILabel *Lbl_Date = (UILabel *)[cell viewWithTag:1000];
    UILabel *Lbl_Score = (UILabel *)[cell viewWithTag:1001];
    UIButton *Btn_Review = (UIButton *)[cell viewWithTag:1002];
    UIButton *Btn_Start = (UIButton *)[cell viewWithTag:1003];
    
    if ([[ArrTests valueForKey:@"is_exit"][indexPath.row] integerValue] == 1 || [[ArrTests valueForKey:@"TestStatus"][indexPath.row] integerValue] == 1) {
        
        [Btn_Review setTitle:@"Review" forState:UIControlStateNormal];
        Btn_Review.tag=indexPath.row;
        [Btn_Review addTarget:self action:@selector(navigateActionSubmit:) forControlEvents:UIControlEventTouchUpInside];
        Lbl_Score.text = [NSString stringWithFormat:@"Score : %@",[ArrTests valueForKey:@"TotalCorrectAnswers"][indexPath.row]];
    }
    else{
        [Btn_Review setTitle:@"Resume" forState:UIControlStateNormal];
        Btn_Review.tag=indexPath.row;
        [Btn_Review addTarget:self action:@selector(navigateAction:) forControlEvents:UIControlEventTouchUpInside];
        Lbl_Score.text = [NSString stringWithFormat:@"Score : N/A"];
        
    }
    Btn_Review.layer.cornerRadius = 5.0;
    Btn_Start.layer.cornerRadius = 5.0;
    
    [Btn_Start setTitle:@"Start Again" forState:UIControlStateNormal];
    Btn_Start.tag=indexPath.row;
    [Btn_Start addTarget:self action:@selector(start_again:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDateFormatter * formatter =  [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * convrtedDate = [formatter dateFromString:[ArrTests valueForKey:@"TestDate"][indexPath.row]];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *dateString = [formatter stringFromDate:convrtedDate];
    
    Lbl_Date.text = [NSString stringWithFormat:@"Date : %@",dateString];
    
    
    
    
    
    return cell;
    
  /*  */
}

-(IBAction)navigateAction:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    //RatingViewController  * next = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingViewController"];
    
    TestQuestionViewController * next = [self.storyboard instantiateViewControllerWithIdentifier:@"TestQuestionViewController"];
    next.GETSTUDY = YES;
    next.GetTestHeaderId = [ArrTests valueForKey:@"TestHDRID"][btn.tag];
    //next.GetTestHeaderId = @"35";
    [self.navigationController pushViewController:next animated:YES];
}

-(IBAction)navigateActionSubmit:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    RatingViewController  * next = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingViewController"];
    
    next.gethaderid = [ArrTests valueForKey:@"TestHDRID"][btn.tag];
    [self.navigationController pushViewController:next animated:YES];
}

-(IBAction)start_again:(id)sender
{
    StudyViewController * next = [self.storyboard instantiateViewControllerWithIdentifier:@"StudyViewController"];
   
    [self.navigationController pushViewController:next animated:YES];
    
}

@end
