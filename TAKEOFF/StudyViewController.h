////
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMCheckBox.h"
#import "SWRevealViewController.h"
#import "SidebarViewController.h"
#import "StudySelectionViewController.h"
#import "TestQuestionViewController.h"

@interface StudyViewController : UIViewController<BEMCheckBoxDelegate,SWRevealViewControllerDelegate>
{
    //WrapperClass Object
    HttpWrapper *httpTest;
    
    NSString *GetCheck;
    NSString *Check;
    BEMCheckBoxGroup*group;
    
    //Outlets
    
    //CheckMark
    IBOutlet BEMCheckBox *CheckFirst;
    IBOutlet BEMCheckBox *CheckSecond;
    IBOutlet BEMCheckBox *CheckThird;
    
    //View
    IBOutlet UIView *Vw_Set;
    
    //Button
    IBOutlet UIButton *Btn_Study;
    IBOutlet UIButton *Btn_Test;
    IBOutlet UIButton *Btn_Side;
    
}

//Property
@property (strong, nonatomic) SWRevealViewController *viewController;

//Action
- (IBAction)StudyButtonPressed:(id)sender;
- (IBAction)TestButtonPressed:(id)sender;
- (IBAction)StartButtonPressed:(id)sender;

@end
