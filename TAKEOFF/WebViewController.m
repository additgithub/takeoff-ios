//
//  WebViewController.m
//  Doctor Pocket
//
//  Created by ADMIN on 11/2/17.
//  Copyright © 2017 ADMIN. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController


#pragma mark - Happy Coding

//ViewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"How It Works ?";
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:46/255.0 green:151/255.0 blue:230/255.0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Semibold" size:17.0]}];

    UIButton *dButton=[UIButton buttonWithType:0];
    dButton.frame=CGRectMake(50,50,50,50);
    
    //    [dButton addTarget:self  action:@selector(clickdButton:)
    //      forControlEvents:UIControlEventTouchUpInside];
    [dButton setImage:[UIImage imageNamed:@"MenuBarColored"]
             forState:UIControlStateNormal];
    dButton.adjustsImageWhenHighlighted=NO;
    dButton.adjustsImageWhenDisabled=NO;
    dButton.tag=0;
    dButton.backgroundColor=[UIColor clearColor];
    
    UIBarButtonItem *RightButton=[[UIBarButtonItem alloc] initWithCustomView:dButton];
    self.navigationItem.leftBarButtonItem=RightButton;
    
    //SideBar Added
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController setDelegate:self];
    if ( revealViewController )
    {
        [dButton addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    Vw_Hader.layer.masksToBounds = NO;
    Vw_Hader.layer.shadowOffset = CGSizeMake(0, 1);
    Vw_Hader.layer.shadowRadius = 0;
    Vw_Hader.layer.shadowOpacity = 0.2;
    
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AboutUS" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [Web_Vw loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    
    [self setupExclude];
}

#pragma mark - SWRevealViewController Delegate

//revealController didMoveToPosition
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (position == FrontViewPositionLeft)
    {
        revealController.frontViewController.view.alpha = 1;
        [self.revealViewController tapGestureRecognizer];
        self.view.userInteractionEnabled = YES;
    }
    else
    {
        revealController.frontViewController.view.alpha = 0.5;
        
        self.view.userInteractionEnabled = NO;
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
}

- (void)setupExclude{
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPG)];
    longPress.minimumPressDuration = 0.2;
    [Web_Vw addGestureRecognizer:longPress];
    
    UITapGestureRecognizer *singleTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:nil];
    singleTapGesture.numberOfTapsRequired = 1;
    singleTapGesture.numberOfTouchesRequired  = 1;
    [Web_Vw addGestureRecognizer:singleTapGesture];
    
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(longPG)];
    doubleTapGesture.numberOfTapsRequired = 2;
    doubleTapGesture.numberOfTouchesRequired = 1;
    [Web_Vw addGestureRecognizer:doubleTapGesture];
    [singleTapGesture requireGestureRecognizerToFail:doubleTapGesture];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    BOOL res = [super canPerformAction:action withSender:sender];
    UIMenuController.sharedMenuController.menuVisible = NO;
    Web_Vw.userInteractionEnabled = NO;
    Web_Vw.userInteractionEnabled = YES;
    return res;
}

- (void)longPG{
    UIMenuController.sharedMenuController.menuVisible = NO;
    Web_Vw.userInteractionEnabled = NO;
    Web_Vw.userInteractionEnabled = YES;
}


#pragma mark - WebView Delegate

//webViewDidFinishLoad
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
}

@end
