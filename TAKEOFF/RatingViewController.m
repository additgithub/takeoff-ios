////
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "RatingViewController.h"
#import "StudyViewController.h"

@interface RatingViewController ()

@end

@implementation RatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Instrument Rating (SCORE)";
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:46/255.0 green:151/255.0 blue:230/255.0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Semibold" size:17.0]}];
    
    [self CallMyMethod];
}

-(void)CallMyMethod
{
    [[AppDelegate sharedAppDelegate] showProgress];
    
    NSString *UserID = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"UserID"];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:_gethaderid forKey:@"test_header_id"];
    [AddPost setValue:UserID forKey:@"user_id"];
    
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpRating  = [[HttpWrapper alloc] init];
        httpRating.delegate=self;
        httpRating.getbool=NO;
      //  [httpRating requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,GETTestDetails] param:[AddPost copy]];
        
        [httpRating requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,GETTestDetailRating] param:[AddPost copy]];
               
        
    });
    
}


#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    NSLog(@"dict:%@",dicsResponse);
    
    [[AppDelegate sharedAppDelegate] hideProgress];
    if ([[dicsResponse valueForKey:@"status"] boolValue] == true) {
        
        if(wrapper == httpRating && httpRating != nil)
        {
            NSArray *data = [dicsResponse valueForKey:@"data"];
            Lbl_Date.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"date"]];
            if ([[NSString stringWithFormat:@"%@",[data valueForKey:@"total_right_question"]] isEqualToString:@""]) {
                Lbl_RightAns.text = [NSString stringWithFormat:@"0"];
            }
            else{
                Lbl_RightAns.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"total_right_question"]];
            }
            
            if ([[NSString stringWithFormat:@"%@",[data valueForKey:@"total_incorrect_question"]] isEqualToString:@""]) {
                Lbl_IncorrectAns.text = [NSString stringWithFormat:@"0"];
            }
            else{
                Lbl_IncorrectAns.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"total_incorrect_question"]];
            }
            
            if ([[NSString stringWithFormat:@"%@",[data valueForKey:@"total_marked_question"]] isEqualToString:@""]) {
                Lbl_MarkedQue.text = [NSString stringWithFormat:@"0"];
            }
            else{
               Lbl_MarkedQue.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"total_marked_question"]];
            }
            
            if ([[NSString stringWithFormat:@"%@",[data valueForKey:@"total_not_attempted"]] isEqualToString:@""]) {
                Lbl_GradePoints.text = [NSString stringWithFormat:@"0"];
            }
            else{
                Lbl_GradePoints.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"total_not_attempted"]];
            }
            
            if ([[NSString stringWithFormat:@"%@",[data valueForKey:@"total_question"]] isEqualToString:@""]) {
                lbl_tot_questions.text = [NSString stringWithFormat:@"0"];
            }
            else{
                lbl_tot_questions.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"total_question"]];
            }
            
            NSInteger getper = [Lbl_RightAns.text integerValue] * 100;
            
            NSInteger gettotal = [[data valueForKey:@"total_question"] integerValue];
            
            NSString *final = [NSString stringWithFormat:@"%ld",getper/gettotal];
            
            NSString *total = [NSString stringWithFormat:@"%@",final];
            NSLog(@"total %@",total);
            Lbl_Percentage.text = [NSString stringWithFormat:@"%@ %%",total];
        }
        else{
            
            if ([[dicsResponse valueForKey:@"status"] integerValue] == 1) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        
    }
    else
    {
        [[AppDelegate sharedAppDelegate] hideProgress];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
    }
}

//fetchDataFail
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
    [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:FAIL_DATA_MSG];
    NSLog(@"Fetch Data Fail Error:%@",error);
}


- (IBAction)btn_BACK:(id)sender {
    
    StudyViewController  * next = [self.storyboard instantiateViewControllerWithIdentifier:@"StudyViewController"];
    
    [self.navigationController pushViewController:next animated:NO];
    
}
@end
