//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//  Cred : ramiz.onesourcewebs@gmail.comm   Pass:admin

#import "LoginViewController.h"
#import "ForgotViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

#pragma mark - Happy Coding
    
//viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSMutableAttributedString *CreateAcc = [[NSMutableAttributedString alloc] initWithString:Lbl_CreateAcc.text attributes:nil];
    
    NSString *AccString = @"New User ? Sign up";
    
    NSRange linkRange2 = [AccString rangeOfString:[NSString stringWithFormat:@"Sign up"]];
    
    NSDictionary *linkAttributes2 = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:46/255.0 green:151/255.0 blue:230/255.0 alpha:1.0]};
    [CreateAcc setAttributes:linkAttributes2 range:linkRange2];
    
    // Assign attributedText to UILabel
    Lbl_CreateAcc.attributedText = CreateAcc;
    Lbl_CreateAcc.userInteractionEnabled = YES;
    [Lbl_CreateAcc addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleCreateAcc)]];
    
    if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
    {
        NSLog(@"iphone5");
        Vw_Login.frame = CGRectMake(Vw_Login.frame.origin.x, Vw_Login.frame.origin.y - 25, Vw_Login.frame.size.width, Vw_Login.frame.size.height + 50);
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
    {
        NSLog(@"iphone4");
        Vw_Login.frame = CGRectMake(Vw_Login.frame.origin.x, Vw_Login.frame.origin.y - 60, Vw_Login.frame.size.width, Vw_Login.frame.size.height + 100);
    }
    
    //Validation-----
    [self setupAlerts];
}

//viewWillAppear
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:true];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}


#pragma mark - Method

// setupAlerts
-(void)setupAlerts{
    [Txt_UserID addRegx:REGEX_EMAIL withMsg:@"Enter valid UserID."];
    
    [Txt_Password addRegx:REGEX_PASSWORD_LIMIT withMsg:@"Password characters limit should be come between 4-20"];
}

// handleCreateAcc
-(void)handleCreateAcc
{
    RegisterViewController *change = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:change animated:YES];
}


#pragma mark - GetAllPosts

-(void)CallMyMethod
{
    [[AppDelegate sharedAppDelegate] showProgress];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:Txt_UserID.text forKey:@"login"];
    [AddPost setValue:Txt_Password.text forKey:@"password"];
    if ([[AppDelegate sharedAppDelegate].deviceT isEqualToString:@""] || [[AppDelegate sharedAppDelegate].deviceT isEqual:[NSNull null]] || [AppDelegate sharedAppDelegate].deviceT == nil){
        [AddPost setValue:@"123" forKey:@"device_id"];
    }else{
        [AddPost setValue:[AppDelegate sharedAppDelegate].deviceT forKey:@"device_id"];
    }
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httplogin  = [[HttpWrapper alloc] init];
        httplogin.delegate=self;
        httplogin.getbool=NO;
        [httplogin requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,LoginAPI] param:[AddPost copy]];
    });
    
}

#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    NSLog(@"dict:%@",dicsResponse);

    [[AppDelegate sharedAppDelegate] hideProgress];
    if ([[dicsResponse valueForKey:@"status"] boolValue] == true) {
        
        NSString *UserID = [dicsResponse valueForKeyPath:@"data.user.id"];
        [[NSUserDefaults standardUserDefaults] setObject:UserID forKey:@"UserID"];
        
        NSString *RoleID = [dicsResponse valueForKeyPath:@"data.user.role_id"];
        [[NSUserDefaults standardUserDefaults] setObject:RoleID forKey:@"RoleID"];
        
        NSString *FirstName = [dicsResponse valueForKeyPath:@"data.user.first_name"];
        [[NSUserDefaults standardUserDefaults] setObject:FirstName forKey:@"FirstName"];
        
        NSString *LastName = [dicsResponse valueForKeyPath:@"data.user.last_name"];
        [[NSUserDefaults standardUserDefaults] setObject:LastName forKey:@"LastName"];
        
        NSString *Email = [dicsResponse valueForKeyPath:@"data.user.email"];
        [[NSUserDefaults standardUserDefaults] setObject:Email forKey:@"Email"];
        
        NSString *LoginToken = [dicsResponse valueForKeyPath:@"data.login_token"];
        [[NSUserDefaults standardUserDefaults] setObject:LoginToken forKey:@"LoginToken"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        StudyViewController *frontViewController = [storyboard instantiateViewControllerWithIdentifier:@"StudyViewController"];
        SidebarViewController *rearViewController = [storyboard instantiateViewControllerWithIdentifier:@"SidebarViewController"];
        
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
        
        UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
        
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
        revealController.delegate = self;
        
        self.viewController = revealController;
        [self.navigationController pushViewController:self.viewController animated:YES];
    }
    else
    {
        [[AppDelegate sharedAppDelegate] hideProgress];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
    }
}

//fetchDataFail
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
    [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:FAIL_DATA_MSG];
    NSLog(@"Fetch Data Fail Error:%@",error);
}


#pragma mark - Button Action
    
//ForgotButtonPressed
- (IBAction)ForgotButtonPressed:(id)sender {
    [self.view endEditing:YES];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ForgotViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"ForgotViewController"];
    [[self navigationController] pushViewController:vc animated:YES];
}
   
//LoginButtonPressed
- (IBAction)LoginButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if ([Txt_UserID validate] || [Txt_Password validate]) {
        [self CallMyMethod];
    }
}

@end
