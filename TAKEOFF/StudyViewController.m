////
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "StudyViewController.h"



@interface StudyViewController ()

@end

@implementation StudyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    //SideBar Added
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController setDelegate:self];
    if ( revealViewController )
    {
        [Btn_Side addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    Check = @"Study";
    Vw_Set.hidden = YES;
    
    //Using RememberMe Checkbox
    CheckFirst.delegate=self;
    CheckFirst.boxType=BEMBoxTypeCircle;
    CheckSecond.delegate=self;
    CheckSecond.boxType=BEMBoxTypeCircle;
    CheckThird.delegate=self;
    CheckThird.boxType=BEMBoxTypeCircle;
    
    //Using RememberMe Checkbox
    group = [BEMCheckBoxGroup groupWithCheckBoxes:@[CheckFirst, CheckSecond, CheckThird]];
    group.selectedCheckBox = CheckFirst; // Optionally set which checkbox is pre-selected
    GetCheck = @"30";
    group.mustHaveSelection = YES; // Define if the group must always have a selection
    

    [Btn_Test setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [Btn_Test setBackgroundColor:[UIColor colorWithRed:48.0/255.0 green:166.0/255.0 blue:255/255 alpha:1]];
    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: Btn_Test.bounds byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii: (CGSize){5.0, 5.0}].CGPath;
    
    Btn_Test.layer.mask = maskLayer1;
    
    [Btn_Study setTitleColor:[UIColor colorWithRed:48.0/255.0 green:166.0/255.0 blue:255/255 alpha:1] forState:UIControlStateNormal];
    [Btn_Study setBackgroundColor:[UIColor whiteColor]];
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: Btn_Study.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft cornerRadii: (CGSize){5.0, 5.0}].CGPath;
    
    Btn_Study.layer.mask = maskLayer;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)CallMyMethod
{
    [[AppDelegate sharedAppDelegate] showProgress];
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpTest  = [[HttpWrapper alloc] init];
        httpTest.delegate=self;
        httpTest.getbool=YES;
        [httpTest requestWithMethod:@"GET" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,GETChapterAPI] param:nil];
    });
    
}

#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    NSLog(@"dict:%@",dicsResponse);
    
    [[AppDelegate sharedAppDelegate] hideProgress];
    if ([[dicsResponse valueForKey:@"status"] boolValue] == true) {
        
    }
    else
    {
        [[AppDelegate sharedAppDelegate] hideProgress];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
    }
}

//fetchDataFail
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
    [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:FAIL_DATA_MSG];
    NSLog(@"Fetch Data Fail Error:%@",error);
}

#pragma mark - SWRevealViewController Delegate

//revealController didMoveToPosition
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft)
    {
        revealController.frontViewController.view.alpha = 1;
        [self.revealViewController tapGestureRecognizer];
        self.view.userInteractionEnabled = YES;
    }
    else
    {
        revealController.frontViewController.view.alpha = 0.5;
        
        self.view.userInteractionEnabled = NO;
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
}

#pragma mark - CheckBox Delegate
//DidTapCheckBox
- (void)didTapCheckBox:(BEMCheckBox*)checkBox
{
    [self.view endEditing:YES];
    if (checkBox==CheckFirst)
    {
        GetCheck = @"30";
    }
    else if (checkBox==CheckSecond)
    {
        GetCheck = @"60";
    }
    else if (checkBox==CheckThird)
    {
        GetCheck = @"100";
    }
}

#pragma mark - Button Action
- (IBAction)StudyButtonPressed:(id)sender {
    Check = @"Study";
    
    [Btn_Test setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [Btn_Test setBackgroundColor:[UIColor colorWithRed:48.0/255.0 green:166.0/255.0 blue:255/255 alpha:1]];
    
    [Btn_Study setTitleColor:[UIColor colorWithRed:48.0/255.0 green:166.0/255.0 blue:255/255 alpha:1] forState:UIControlStateNormal];
    [Btn_Study setBackgroundColor:[UIColor whiteColor]];
    
    
    
    Vw_Set.hidden = YES;
}

- (IBAction)TestButtonPressed:(id)sender {
    Check = @"Test";
    [Btn_Study setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [Btn_Study setBackgroundColor:[UIColor colorWithRed:48.0/255.0 green:166.0/255.0 blue:255/255 alpha:1]];
    
    [Btn_Test setTitleColor:[UIColor colorWithRed:48.0/255.0 green:166.0/255.0 blue:255/255 alpha:1] forState:UIControlStateNormal];
    [Btn_Test setBackgroundColor:[UIColor whiteColor]];
   
    Vw_Set.hidden = NO;
}

- (IBAction)StartButtonPressed:(id)sender {

    if ([Check isEqualToString:@"Study"]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        StudySelectionViewController *frontViewController = [storyboard instantiateViewControllerWithIdentifier:@"StudySelectionViewController"];
        SidebarViewController *rearViewController = [storyboard instantiateViewControllerWithIdentifier:@"SidebarViewController"];
        
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
        
        UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
        
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
        revealController.delegate = self;
        
        self.viewController = revealController;
        [self.navigationController pushViewController:self.viewController animated:YES];
    }
    else{
        TestQuestionViewController * next = [self.storyboard instantiateViewControllerWithIdentifier:@"TestQuestionViewController"];
        next.GETSTUDY = NO;
        next.GetChapterID = GetCheck;
        [self.navigationController pushViewController:next animated:YES];
        
        
        //NewTestVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewTestVC"];
        //next.GETSTUDY = NO;
        //next.GetChapterID = GetCheck;
        //[self.navigationController pushViewController:next animated:YES];
    }
    
    
    
}
@end
