//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Change Password";
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:46/255.0 green:151/255.0 blue:230/255.0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Semibold" size:17.0]}];
    
    UIButton *dButton=[UIButton buttonWithType:0];
    dButton.frame=CGRectMake(50,50,50,50);
    [dButton setImage:[UIImage imageNamed:@"MenuBarColored"]
             forState:UIControlStateNormal];
    dButton.adjustsImageWhenHighlighted=NO;
    dButton.adjustsImageWhenDisabled=NO;
    dButton.tag=0;
    dButton.backgroundColor=[UIColor clearColor];
    
    UIBarButtonItem *RightButton=[[UIBarButtonItem alloc] initWithCustomView:dButton];
    self.navigationItem.leftBarButtonItem=RightButton;
    
    //SideBar Added
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController setDelegate:self];
    if ( revealViewController )
    {
        [dButton addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    Vw_Back.layer.masksToBounds = NO;
    Vw_Back.layer.shadowOffset = CGSizeMake(0, 0);
    Vw_Back.layer.shadowRadius = 0.5;
    Vw_Back.layer.shadowOpacity = 0.5;
    
    [self setupAlerts];
}




#pragma mark - SWRevealViewController Delegate

//revealController didMoveToPosition
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (position == FrontViewPositionLeft)
    {
        revealController.frontViewController.view.alpha = 1;
        [self.revealViewController tapGestureRecognizer];
        self.view.userInteractionEnabled = YES;
    }
    else
    {
        revealController.frontViewController.view.alpha = 0.5;
        
        self.view.userInteractionEnabled = NO;
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
}


#pragma mark - Method

// setupAlerts
-(void)setupAlerts{
    [Txt_CurrentPass addRegx:REGEX_PASSWORD_LIMIT withMsg:@"Please enter valid current password."];
    [Txt_ConfirmPass addRegx:REGEX_PASSWORD_LIMIT withMsg:@"Please enter valid confirm password."];
    [Txt_NewPass addRegx:REGEX_PASSWORD_LIMIT withMsg:@"Please enter valid new password."];
}

#pragma mark - GetAllPosts

-(void)CallMyMethod
{
    [[AppDelegate sharedAppDelegate] showProgress];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:Txt_CurrentPass.text forKey:@"old_password"];
    [AddPost setValue:Txt_NewPass.text forKey:@"new_password"];
    [AddPost setValue:Txt_ConfirmPass.text forKey:@"confirm_new_password"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpchange  = [[HttpWrapper alloc] init];
        httpchange.delegate=self;
        httpchange.getbool=NO;
        [httpchange requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,ChangeAPI] param:[AddPost copy]];
    });
}

#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    NSLog(@"dict:%@",dicsResponse);
    
    [[AppDelegate sharedAppDelegate] hideProgress];
    if ([[dicsResponse valueForKey:@"status"] boolValue] == true) {
        
        Txt_NewPass.text = @"";
        Txt_ConfirmPass.text = @"";
        Txt_CurrentPass.text = @"";
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
    }
    else
    {
        [[AppDelegate sharedAppDelegate] hideProgress];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
    }
}

//fetchDataFail
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
    [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:FAIL_DATA_MSG];
    NSLog(@"Fetch Data Fail Error:%@",error);
}


- (IBAction)ChangeButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if ([Txt_CurrentPass validate] || [Txt_ConfirmPass validate] || [Txt_NewPass validate]) {
        [self CallMyMethod];
    }
}
@end
