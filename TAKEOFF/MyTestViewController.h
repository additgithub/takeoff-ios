////
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "HttpWrapper.h"
#import "AppDelegate.h"
#import "RatingViewController.h"

@interface MyTestViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpWrapperDelegate>
{
    NSMutableArray *ArrTests;
    
    //WrapperClass Object
    HttpWrapper *httpTest;
    
    //Outlets
    
    //Views
    IBOutlet UITableView *Tbl_Main;
    
    //Button
    IBOutlet UIButton *Btn_Side;
}

//Action


@end
