//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "AppDelegate.h" //AppDelegate File
#import "ApplicationConstant.h" // Constant File
#include <sys/utsname.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "TextFieldValidator.h"


@import MobileCoreServices;    // only needed in iOS

@interface ApplicationData : NSObject{
}

@property (retain, nonatomic) NSString *IsFree;
@property (retain, nonatomic) NSString *ConsultId;
@property (retain, nonatomic) NSString *DoctorImage;
@property (retain, nonatomic) NSString *DoctorResponceTime;
@property (retain, nonatomic) NSString *DoctorFirstName;
@property (retain, nonatomic) NSString *DoctorLastName;
@property (retain, nonatomic) NSString *DoctorLocation;
@property (retain, nonatomic) NSString *DoctorId;
@property (retain, nonatomic) NSString *DoctorCharge;
@property (retain, nonatomic) NSString *DoctorChargeCurrency;
@property (retain, nonatomic) NSString *DoctorName;
@property (retain, nonatomic) NSString *DoctorSpeciality;
@property (retain, nonatomic) NSString *DoctorAbout;
@property BOOL ConsultEnded;
@property NSInteger *LastSelectTab;
@property (retain, nonatomic) NSString *LastSelectTabName;
@property (retain, nonatomic) NSString *ChatDoctorName;
@property (retain, nonatomic) NSString *WalletAmount;

@property (retain, nonatomic) NSString *NoteAmount;
@property (retain, nonatomic) NSString *UnPaidCount;
@property BOOL APPFIRSTTIME;


//ImageCropping



-(UIImage *)BackToImage : (UIImage *)Image : (UIImageView *)ImageView;
- (void)updateCirclePathAtLocation:(CGPoint)location radius:(CGFloat)radius imageview:(UIImageView *)ImageView;

@property (retain, nonatomic) NSMutableArray *SyptomsIndex;

+ (ApplicationData*)sharedInstance;//Create a Delegate

-(BOOL)ValidEmail:(NSString *)checkString; //Check Valid Email Or Not

-(UIView *)GradiantColorUsing :(UIView*)View;

-(void)ShowMainView;

@end
