//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "ApplicationData.h"

static ApplicationData *applicationData = nil;

@implementation ApplicationData
@synthesize ConsultId;

//Create a Delegate
- (void)initialize {
}

+ (ApplicationData*)sharedInstance
{
    if (applicationData == nil)
    {
        applicationData = [[super allocWithZone:NULL] init];
        [applicationData initialize];
    }
    return applicationData;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedInstance];
}

- (id)init {
    if(self = [super init])
    {
        self.SyptomsIndex = [[NSMutableArray alloc] init];
    }
    return self;
}


//Valid Email CheckString
-(BOOL)ValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(UIView *)GradiantColorUsing :(UIView*)View
{
    //Gradiant View
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = View.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:46/255.0 green:190/255.0 blue:242/255.0 alpha:1].CGColor, (id)[UIColor colorWithRed:22/255.0 green:155/255.0 blue:210/255.0 alpha:1].CGColor];
    
    [View.layer insertSublayer:gradient atIndex:0];
    
    return View;
}


@end
