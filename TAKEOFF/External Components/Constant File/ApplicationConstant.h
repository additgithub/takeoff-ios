//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "ApplicationConstant.h"
#import "ApplicationData.h"

#ifndef TAKEOFF_h
#define TAKEOFF_h

#ifndef __IPHONE_5_0
#warning "This project uses features only available in iOS SDK 5.0 and later."
#endif

//Test Cred

#define JsonUrlConstant @"https://takeoff.blenzabi.com/api/v1/web_services/"

#define Register @"register"
#define LoginAPI @"login"
#define ForgotAPI @"forgot_password"
#define ChangeAPI @"change_password"
#define ProfileAPI @"profile"
#define GETChapterAPI @"get_chapter"
#define GETStudyQuestionAPI @"get_study_questions"
#define GETTestQuestionAPI @"get_test_questions"
#define MYTESTLIST @"my_test_list"
#define GETTESTDETAIL @"get_test_detail"
#define SubmitTest @"submit_test"
#define GETTestDetails @"get_test_detail"
#define GETTestDetailRating @"get_test_detail_rating"

#define VersionNumber @"1" //Version Number
#define DEVICE_TOKEN @"devicetoken" //Device Token

//PaypalAccountProduction
#define PayPalEnvironmentProduction_Key @"AXORmeUOWK9QW0hw68V6iO7kZMKYpasXqZiG6PaiwMrDJTs9oSc13g5uyRdoNi7mDl77OAtI4FGbhZwx"
#define PayPalEnvironmentSandbox_Key @"AXkBxqsh-aV0PW0wvYBszG92RjzQGOGkYnERWMGZvGsAmbW-xrGilsPVkTSDD_RWZf6tyrikOX6l0W03"


//MESSAGES

//GoogleClientId
#define kClientId @"436674102927-54u5mf0sqovisr2r8t0l76gejv2nls5j.apps.googleusercontent.com"

//Normal Alerts
#define ERROR_HEADER @"Hey there!"

//Alert Messages
#define FAIL_DATA_MSG @"Something went wrong. please try again later."

#define TOKENSTRING         @"testermanishrahul234142test"
#define DEVICENAME          @"iphone"
#define kGCMMessageIDKey    @"gcm.message_id"

#define iphone4 [UIScreen mainScreen].bounds.size.height == 480
#define placeHolderImage @"ic_avatar.png"
#define Font @"Trebuchet MS"
#define NurseConverstionMessage @"Are you sure want to continue?"

//Alert Define
#define Alert(title,msg)  [[[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Continue" otherButtonTitles:nil, nil]  show];

//Validation Alerts
#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_Note_USER_NAME @"[A-Za-z0-9_ ]{3,40}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{4,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{6,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{10}"
#define REGEX_OTP_DEFAULT @"[0-9]{6}"
#define REGEX_AGE_DEFAULT @"[0-9]{1,2}"

#endif
