//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"

@class ASIFormDataRequest,HttpWrapper;

@protocol HttpWrapperDelegate

@optional

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse;
- (void) HttpWrapper1:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse;
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error;

- (void) fetchImageSuccess:(NSString *)response;
- (void) fetchImageFail:(NSError *)error;
- (void) fetchDataSuccess:(NSString *)response;
- (void) fetchDataFail:(NSError *)error;

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchsuccesswithVideoData:(NSData *)videoData;
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchsuccesswithVideoUrl:(NSString *)videoUrlString;


@end

@interface HttpWrapper : NSObject
{
    AFHTTPSessionManager *operation;
    NSMutableDictionary *dics;
}

@property (nonatomic, assign) ASIFormDataRequest *requestMain;
@property (nonatomic, assign) NSObject<HttpWrapperDelegate> *delegate;
@property (nonatomic ,strong) NSString *CreditCardType;
@property (assign, nonatomic) BOOL getbool;
@property (assign, nonatomic) BOOL Strip;

-(void) requestWithMethod:(NSString*)method url:(NSString*)strUrl param:(NSMutableDictionary*)dictParam;
-(void) cancelRequest;

@end
