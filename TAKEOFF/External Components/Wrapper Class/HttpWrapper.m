//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "HttpWrapper.h"
#import "AppDelegate.h"

@implementation HttpWrapper

AppDelegate *appDelegate;

@synthesize requestMain;
@synthesize delegate;

-(id)init
{
    self = [super init];
    if(self)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        });
        
    }
    return self;
}

-(void) requestWithMethod:(NSString*)method url:(NSString*)strUrl param:(NSMutableDictionary*)dictParam
{
    NSLog(@"dict is %@",dictParam);
    if(requestMain)
    {
        requestMain = nil;
    }
    //setAllHTTPHeaderFields
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"admin" password:@"API@TAKEOFF!#$WEB$"];
    
    [manager.requestSerializer setValue:@"kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss" forHTTPHeaderField:@"X-TAKEOFF-API-KEY"];
    [manager.requestSerializer setValue:@"Basic YWRtaW46QVBJQFRBS0VPRkYhIyRXRUIk" forHTTPHeaderField:@"authorization"];
    
    //0w8o8w0s80kwgcs4csco844s08ggkw4c
    //kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss
    
    NSString *UserID = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"UserID"];
    NSString *LoginToken = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"LoginToken"];
    
    if ( ( ( ![UserID isEqual:[NSNull null]] ) && ( [UserID length] != 0 ) ) || ( ( ![LoginToken isEqual:[NSNull null]] ) && ( [LoginToken length] != 0 ) )) {
        [manager.requestSerializer setValue:LoginToken forHTTPHeaderField:@"X-TAKEOFF-LOGIN-TOKEN"];
        [manager.requestSerializer setValue:UserID forHTTPHeaderField:@"USER-ID"];
    }
    
  //  manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //NSLog(@"---%@",AddPost);//get_invited_people_list
    
    if (_getbool == YES)
    {
        [manager GET:strUrl parameters:dictParam success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            if (delegate != nil)
            {
                NSLog(@"dictparam %@",dictParam);
                
                if([delegate respondsToSelector:@selector(fetchDataSuccess:)])
                {
                    [delegate performSelector:@selector(fetchDataSuccess:) withObject:responseObject];
                }
                
                if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataSuccess:)])
                {
                    [delegate HttpWrapper:self fetchDataSuccess:responseObject];
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            if (delegate == nil)
                return;
            
            if([delegate respondsToSelector:@selector(fetchDataFail:)])
                [delegate performSelector:@selector(fetchDataFail:) withObject:error];
            
            if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataFail:)])
                [delegate HttpWrapper:self fetchDataFail:error];
        }];
    }
    else{
        [manager POST:strUrl parameters:dictParam success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            if (delegate != nil)
            {
                NSLog(@"dictparam %@",dictParam);
                
                if([delegate respondsToSelector:@selector(fetchDataSuccess:)])
                {
                    [delegate performSelector:@selector(fetchDataSuccess:) withObject:responseObject];
                }
                
                if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataSuccess:)])
                {
                    [delegate HttpWrapper:self fetchDataSuccess:responseObject];
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            
            NSMutableDictionary *resp = operation.responseObject;
            
            if([[resp valueForKey:@"status"] isEqual:@"false"]){
               NSMutableDictionary  *resultDic= [resp valueForKey:@"data"];
               // NSMutableDictionary  *resultDic = data[0];
                NSArray *values = [resultDic allValues];
                NSString *message =@"";
                if ([values count] != 0)
                    message = [values objectAtIndex:0];
                [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
                [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:message];
                return;
            }
           
            if(operation.response)
            if (delegate == nil)
                return;
            
            if([delegate respondsToSelector:@selector(fetchDataFail:)])
                [delegate performSelector:@selector(fetchDataFail:) withObject:error];
            
            if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataFail:)])
                [delegate HttpWrapper:self fetchDataFail:error];
        }];
    }
   
}


-(void) cancelRequest
{
//    [operation.operationQueue cancelAllOperations];
//    requestMain.delegate = nil;
//    [requestMain cancel];
}
@end
