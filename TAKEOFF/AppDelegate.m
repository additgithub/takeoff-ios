//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

#pragma mark - SharedAppDelegate

//SharedAppDelegate
+(AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
  //  [[UIApplication sharedApplication] setStatusBarHidden:false];
    
   // [[UIApplication sharedApplication] setStatusBarHidden:false];
    
    
    
  //  UIView *statusBar=[[UIApplication sharedApplication] valueForKey:@"statusBar"];
 //   statusBar.backgroundColor = [UIColor whiteColor];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    //Keyboard Integration
    [[IQKeyboardManager sharedManager] setEnable:YES];
//    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
//    IQKeyboardManager.sharedManager.shouldShowTextFieldPlaceholder = YES;
//    IQKeyboardManager.sharedManager.shouldResignOnTouchOutside = YES;
//    IQKeyboardManager.sharedManager.keyboardDistanceFromTextField = 50.0;
    
    
    NSString *UserID = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"UserID"];
    
    [FIRApp configure];
    
    if ( ( ![UserID isEqual:[NSNull null]] ) && ( [UserID length] != 0 ) )  {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        StudyViewController *frontViewController = [storyboard instantiateViewControllerWithIdentifier:@"StudyViewController"];
        SidebarViewController *rearViewController = [storyboard instantiateViewControllerWithIdentifier:@"SidebarViewController"];
        
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
        
        UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
        
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
        revealController.delegate = self;
        
        self.viewController = revealController;
        self.window.rootViewController = self.viewController;
        [self.window makeKeyAndVisible];
        
    }
    else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginViewController *Login = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:Login];
        self.window.rootViewController = rearNavigationController;
        [self.window makeKeyAndVisible];
    }
    
    return YES;
}

#pragma mark - Extra

-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

//showProgress
-(void) showProgress
{
    UIView*view = [[UIView alloc] initWithFrame:self.window.bounds];
    [view setBackgroundColor:[UIColor blackColor]];
    view.tag = 4444;
    [view setAlpha:0.6];
    
    act = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeBallScaleRippleMultiple tintColor:[UIColor whiteColor]];
    
    [act startAnimating];
    
    [view addSubview:act];
    act.center = view.center;
    [self.window addSubview:view];
}

//hideProgress
-(void) hideProgress
{
    [act stopAnimating];
    for(UIView *view in self.window.subviews)
    {
        if(view.tag==4444)
        {
            [view removeFromSuperview];
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
