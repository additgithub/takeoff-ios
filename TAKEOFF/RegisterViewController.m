//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSMutableAttributedString *CreateAcc = [[NSMutableAttributedString alloc] initWithString:Lbl_Login.text attributes:nil];
    
    NSString *AccString = @"Already have an account? - Login";
    
    NSRange linkRange2 = [AccString rangeOfString:[NSString stringWithFormat:@"Login"]];
    
    NSDictionary *linkAttributes2 = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:46/255.0 green:151/255.0 blue:230/255.0 alpha:1.0]};
    [CreateAcc setAttributes:linkAttributes2 range:linkRange2];
    
    // Assign attributedText to UILabel
    Lbl_Login.attributedText = CreateAcc;
    Lbl_Login.userInteractionEnabled = YES;
    [Lbl_Login addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleCreateAcc)]];
    
    [self DatePickert];
    
    if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
    {
        NSLog(@"iphone5");
        Vw_Back.frame = CGRectMake(Vw_Back.frame.origin.x, Vw_Back.frame.origin.y - 10, Vw_Back.frame.size.width, Vw_Back.frame.size.height + 50);
        Img_Vw.frame = CGRectMake(Img_Vw.frame.origin.x, Img_Vw.frame.origin.y - 20, Img_Vw.frame.size.width, Img_Vw.frame.size.height);
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
    {
        NSLog(@"iphone4");
        Vw_Back.frame = CGRectMake(Vw_Back.frame.origin.x, Vw_Back.frame.origin.y, Vw_Back.frame.size.width, Vw_Back.frame.size.height + 100);
        Img_Vw.frame = CGRectMake(Img_Vw.frame.origin.x, Img_Vw.frame.origin.y - 20, Img_Vw.frame.size.width, Img_Vw.frame.size.height);
        
        Scroll_Vw.contentSize = CGSizeMake(self.view.frame.size.width, 500);
    }
    
    Scroll_Vw.contentSize = CGSizeMake(self.view.frame.size.width, 500);
    
//    Txt_Firstname.text = @"Mike";
//    Txt_LastName.text = @"Tyson";
//    Txt_Username.text = @"MikeTyson"; Txt_Birthdate.text = @"1999-05-02";
//    Txt_Email.text = @"Mike@gmail.com";
}

-(void)handleCreateAcc
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Button
- (IBAction)BrowseButtonPressed:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *alrtconditon=   [UIAlertController alertControllerWithTitle:@"Profile Image" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* tp = [UIAlertAction
                         actionWithTitle:@"Take Photo"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             ipc= [[UIImagePickerController alloc] init];
                             ipc.delegate = self;
                             ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
                             ipc.allowsEditing = YES;
                             [self presentViewController:ipc animated:YES completion:nil];
                             [alrtconditon dismissViewControllerAnimated:YES completion:nil];                             
                         }];
    
    [alrtconditon addAction:tp];
    
    UIAlertAction* cg = [UIAlertAction
                         actionWithTitle:@"Choose from Gallery"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             ipc= [[UIImagePickerController alloc] init];
                             ipc.delegate = self;
                             ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                             ipc.allowsEditing = YES;
                             [self presentViewController:ipc animated:YES completion:nil];
                             [alrtconditon dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    [alrtconditon addAction:cg];
    
    UIAlertAction* can = [UIAlertAction
                          actionWithTitle:@"Cancel"
                          style:UIAlertActionStyleCancel
                          handler:^(UIAlertAction * action)
                          {
                              [alrtconditon dismissViewControllerAnimated:YES completion:nil];
                              
                          }];
    
    [alrtconditon addAction:can];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:alrtconditon];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        [self presentViewController:alrtconditon animated:YES completion:nil];
    }
}

- (IBAction)RegisterButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if ([Txt_Firstname validate] || [Txt_LastName validate]|| [Txt_Username validate] || [Txt_Email validate]) {
        if (![Txt_Birthdate.text isEqualToString:@""]) {
             [self CallMyMethod];
        }
        else{
            [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:@"Please Select Birthdate"];
        }
    }
}


#pragma mark - Date Picker
-(void)DatePickert
{
    NSDate *date = [NSDate date];
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"MM/dd/yyyy"];
    NSString *result = [df stringFromDate:date];
    
    // alloc/init your date picker, and (optional) set its initial date
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]]; //this returns today's date
    
    NSString *maxDateString = result;
    // the date formatter used to convert string to date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // the specific format to use
    dateFormatter.dateFormat = @"MM-dd-yyyy";
    // converting string to date
    NSDate *theMaximumDate = [dateFormatter dateFromString: maxDateString];
    [datePicker setMaximumDate:theMaximumDate]; //the min age restriction
    
    // set the mode
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    
    // update the textfield with the date everytime it changes with selector defined below
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    
    // and finally set the datePicker as the input mode of your textfield
    [Txt_Birthdate setInputView:datePicker];
    
}

#pragma mark - Picker Delegate
-(void)updateTextField:(id)sender {
    
    UIDatePicker *picker = (UIDatePicker*)Txt_Birthdate.inputView;
    Txt_Birthdate.text = [self formatDate:picker.date];
}

- (NSString *)formatDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

#pragma mark - Method

// setupAlerts
-(void)setupAlerts{
    [Txt_Email addRegx:REGEX_EMAIL withMsg:@"Enter valid UserID."];
    [Txt_Firstname addRegx:REGEX_USER_NAME_LIMIT withMsg:@"Please enter valid firstname."];
    [Txt_LastName addRegx:REGEX_USER_NAME_LIMIT withMsg:@"Please enter valid lastname."];
    [Txt_Username addRegx:REGEX_USER_NAME_LIMIT withMsg:@"Please enter valid username."];
}

#pragma mark - GetAllPosts

-(void)CallMyMethod
{
    [[AppDelegate sharedAppDelegate] showProgress];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:Txt_Firstname.text forKey:@"first_name"];
    [AddPost setValue:Txt_LastName.text forKey:@"last_name"];
    [AddPost setValue:Txt_Username.text forKey:@"username"];
    [AddPost setValue:Txt_Email.text forKey:@"email"];
    [AddPost setValue:Txt_Birthdate.text forKey:@"birthdate"];
    /*if ([[AppDelegate sharedAppDelegate].deviceT isEqualToString:@""] || [[AppDelegate sharedAppDelegate].deviceT isEqual:[NSNull null]] || [AppDelegate sharedAppDelegate].deviceT == nil){
     [AddPost setValue:@"0" forKey:@"device_id"];
     }else{
     [AddPost setValue:[AppDelegate sharedAppDelegate].deviceT forKey:@"device_id"];
     }*/
    
   
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            Txt_Firstname.text, @"first_name",
                            Txt_LastName.text, @"last_name",
                            Txt_Username.text, @"username",
                            Txt_Email.text, @"email",
                            Txt_Birthdate.text, @"birthdate",
                            nil];
    
    
    
    NSLog(@" url is %@login",JsonUrlConstant);
    /*
    if (image != nil ) {
        //Create manager
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        //Now post
        NSLog(@"AddPost %@",params);
        NSString *url =[NSString stringWithFormat:@"%@%@",JsonUrlConstant,Register];
        [manager POST:url parameters:[params copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //add img data one by one
            
            NSData *imageData = UIImageJPEGRepresentation(image,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"user_image"] fileName:[NSString stringWithFormat:@"ProfilePic.jpg"] mimeType:@"image/jpeg"];
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            StudyViewController *Study = [storyboard instantiateViewControllerWithIdentifier:@"StudyViewController"];
            [self.navigationController pushViewController:Study animated:YES];
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"Error: %@",error );
            
            [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
            [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:error._IQDescription];
          
            
        }];
    }
    else
    {   */
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httplogin  = [[HttpWrapper alloc] init];
        httplogin.delegate=self;
        httplogin.getbool=NO;
        [httplogin requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,Register] param:[params copy]];
    });
    //}
    
}

#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    NSLog(@"dict:%@",dicsResponse);
    
    [[AppDelegate sharedAppDelegate] hideProgress];
    if ([[dicsResponse valueForKey:@"status"] boolValue] == true) {
        
        /*NSString *UserID = [dicsResponse valueForKeyPath:@"data.user.id"];
         [[NSUserDefaults standardUserDefaults] setObject:UserID forKey:@"UserID"];
         
         NSString *RoleID = [dicsResponse valueForKeyPath:@"data.user.role_id"];
         [[NSUserDefaults standardUserDefaults] setObject:RoleID forKey:@"RoleID"];
         
         NSString *FirstName = [dicsResponse valueForKeyPath:@"data.user.first_name"];
         [[NSUserDefaults standardUserDefaults] setObject:FirstName forKey:@"FirstName"];
         
         NSString *LastName = [dicsResponse valueForKeyPath:@"data.user.last_name"];
         [[NSUserDefaults standardUserDefaults] setObject:LastName forKey:@"LastName"];
         
         NSString *Email = [dicsResponse valueForKeyPath:@"data.user.email"];
         [[NSUserDefaults standardUserDefaults] setObject:Email forKey:@"Email"];
         
         NSString *LoginToken = [dicsResponse valueForKeyPath:@"data.login_token"];
         [[NSUserDefaults standardUserDefaults] setObject:LoginToken forKey:@"LoginToken"];
         
         [[NSUserDefaults standardUserDefaults] synchronize];*/
        
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:@"Success" message:@"Register succesfully"];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginViewController *login = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self.navigationController pushViewController:login animated:YES];
        
        
    }
    else
    {
        [[AppDelegate sharedAppDelegate] hideProgress];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
    }
}

//fetchDataFail
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
    [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:FAIL_DATA_MSG];
    NSLog(@"Fetch Data Fail Error:%@",error);
}

#pragma mark - ImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //NSURL *imageURL = info[UIImagePickerControllerReferenceURL];
    //Txt_Upload.text = imageURL.lastPathComponent;
    
    image = nil;
    image = [info objectForKey:UIImagePickerControllerEditedImage];
    if(image==nil)
    {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        
    }
    if(image==nil)
    {
        image = [info objectForKey:UIImagePickerControllerCropRect];
        
    }
    
    Btn_CheckMark.hidden = NO;
    Btn_CheckMark.layer.cornerRadius = 2.0;
    Btn_CheckMark.layer.masksToBounds = YES;
    
    //Img_User.image = image;
    [ipc dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [ipc dismissViewControllerAnimated:YES completion:nil];
}
@end
