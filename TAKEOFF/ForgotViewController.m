//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import "ForgotViewController.h"

@interface ForgotViewController ()

@end

@implementation ForgotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Forgot Password";
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:46/255.0 green:151/255.0 blue:230/255.0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Semibold" size:17.0]}];
    
    Vw_Back.layer.masksToBounds = NO;
    Vw_Back.layer.shadowOffset = CGSizeMake(0, 0);
    Vw_Back.layer.shadowRadius = 0.5;
    Vw_Back.layer.shadowOpacity = 0.5;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 568)
    {
        Vw_Back.frame = CGRectMake(Vw_Back.frame.origin.x, Vw_Back.frame.origin.y, Vw_Back.frame.size.width, Vw_Back.frame.size.height + 70);
    }
    
    [self setupAlerts];
}

#pragma mark - Method

// setupAlerts
-(void)setupAlerts{
    [Txt_UserID addRegx:REGEX_EMAIL withMsg:@"Enter valid UserID."];
}

#pragma mark - GetAllPosts

-(void)CallMyMethod
{
    [[AppDelegate sharedAppDelegate] showProgress];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:Txt_UserID.text forKey:@"email"];
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpforgot  = [[HttpWrapper alloc] init];
        httpforgot.delegate=self;
        httpforgot.getbool=NO;
        [httpforgot requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@%@",JsonUrlConstant,ForgotAPI] param:[AddPost copy]];
    });
    
}

#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    NSLog(@"dict:%@",dicsResponse);
    
    [[AppDelegate sharedAppDelegate] hideProgress];
    if ([[dicsResponse valueForKey:@"status"] boolValue] == true) {
        
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
        [self.navigationController popViewControllerAnimated:true];
    }
    else
    {
        [[AppDelegate sharedAppDelegate] hideProgress];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:[dicsResponse valueForKey:@"message"]];
    }
}

//fetchDataFail
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
    [[AppDelegate sharedAppDelegate] showAlertWithTitle:ERROR_HEADER message:FAIL_DATA_MSG];
    NSLog(@"Fetch Data Fail Error:%@",error);
}


- (IBAction)SendButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if ([Txt_UserID validate]) {
        [self CallMyMethod];
    }
}

@end
