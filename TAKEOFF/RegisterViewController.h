//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"
#import "SidebarViewController.h"
#import "TextFieldValidator.h"

@interface RegisterViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate,HttpWrapperDelegate>
{
    
    //WrapperClass Object
    HttpWrapper *httplogin;
    
    //Image
    UIImage *image;
    
    //ImagePicker
    UIImagePickerController*ipc;
    
    //Outlets
    
    //Label
    IBOutlet UILabel *Lbl_Login;
    
    //DatePicker
    UIDatePicker *datePicker;
    UIPickerView *pickerView;
    NSMutableArray*years;
    
    //TextFeild
    IBOutlet TextFieldValidator *Txt_Firstname;
    IBOutlet TextFieldValidator *Txt_LastName;
    IBOutlet UITextField *Txt_Upload;
    IBOutlet TextFieldValidator *Txt_Username;
    IBOutlet UITextField *Txt_Birthdate;
    IBOutlet TextFieldValidator *Txt_Email;
    
    //Imageview
    IBOutlet UIImageView *Img_Vw;
    
    IBOutlet UIView *Vw_Back;
    IBOutlet UIScrollView *Scroll_Vw;
    
    //Button
    IBOutlet UIButton *Btn_CheckMark;
    
}

@property (strong, nonatomic) SWRevealViewController *viewController;

//Action
- (IBAction)BrowseButtonPressed:(id)sender;
- (IBAction)RegisterButtonPressed:(id)sender;


@end
