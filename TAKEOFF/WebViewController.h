//
//  WebViewController.h
//  Doctor Pocket
//
//  Created by ADMIN on 11/2/17.
//  Copyright © 2017 ADMIN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationConstant.h" // Constant File

@interface WebViewController : UIViewController <UIWebViewDelegate,SWRevealViewControllerDelegate>
{
    //Outlets
    
    //View
    IBOutlet UIWebView *Web_Vw;
    IBOutlet UIView *Vw_Hader;
    
    //Lable
    IBOutlet UILabel *Lbl_Title;
    
    //Button
    IBOutlet UIButton *Btn_Back;
    
}

@end
