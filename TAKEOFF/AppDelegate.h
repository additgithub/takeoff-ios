//
//  TAKEOFF
//
//  Created by Priyank Jotangiya✌🏻🤓 on 10/6/18.
//  Copyright © 2018 TAKEOFF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <IQKeyboardManager/IQKeyboardManager.h> //Keyboard Manager
#import "DGActivityIndicatorView.h" // Activity Indicator 3rd party
#import "LoginViewController.h"
#import "ApplicationData.h"
@import Firebase;
@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    //Activity Indicator
    DGActivityIndicatorView * act;
}

@property (strong, nonatomic) SWRevealViewController *viewController;
@property (strong, nonatomic) UIWindow *window;

//Shared Prefernce
+(AppDelegate *)sharedAppDelegate;

//ProgressBar
-(void) showProgress;
-(void) hideProgress;

//AlertController
-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message;

//DeviceToken
@property (retain, nonatomic) NSString *deviceT;

@end

